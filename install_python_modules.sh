#~/bin/bash

conda install numpy
conda install pymongo
conda install pandas
conda install scipy
conda install scikit-learn
pip install munkres
conda install -c https://conda.anaconda.org/anaconda openpyxl
