# Epic Phenotyper #

Your data should be like the format in /data_csv_processed/

        PATIENT_NAME, EVENT_TYPE_1_NAME, EVENT_TYPE_2_NAME, VALUE


To run it:

        $ cd pipeline_code
        $ sh run_config.sh

