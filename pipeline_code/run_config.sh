#!/bin/bash

## generate folder to hold stuff for this run:
EVENT_SEQ_LABEL="NAME_FOR_YOUR_EXPT"
EVENT_SEQ_FILE="../data_csv_processed/data_sample.csv"
EXPT_MULT_RANK=1
RANK_RANGE="1 10" ##specify range of RANK if EXPT_MULT_RANK=1
ALPHA=1
GAMMA="0.001,0.12,0.07"
AGGREGATION_METHOD="MEAN" #can use MEAN or COUNT
SAVE_FOLDER="../pipeline_runs/NAME_FOR_YOUR_EXPT_alpha_1_gamma_0.001_0.12_0.07"
EVENT_SEQ_TYPE=2 # 1= event file with timestamp included; 2 = w/o timestamps; just use 2 for now
MODES="PATIENT DIAG MED"


## make output folders
mkdir $SAVE_FOLDER
mkdir $SAVE_FOLDER/event_sequence_processed
mkdir $SAVE_FOLDER/data_tensor
mkdir $SAVE_FOLDER/results
mkdir $SAVE_FOLDER/results/NTF_files

## build event sequence files per day 
# in the pipeline, we would run the build_event_seq_file_per_chunk.py. However, in this case we do NOT do that becuase there is only ONE FILE


## in the pipeline, we would loop through the days, and run the pipeline steps
## however, in here we just have one big event sequence file so just 1. build tensor and 2. run marble and analyze marble, using the one file
THIS_EVENT_SEQ_FILE=$EVENT_SEQ_LABEL
THIS_EVENT_SEQ_FILE_LOCATION=$EVENT_SEQ_FILE
## 1. construct tensor 
COMMAND="python build_tensor.py -i "$THIS_EVENT_SEQ_FILE_LOCATION" -o "$SAVE_FOLDER/data_tensor/$THIS_EVENT_SEQ_FILE-{0}.dat" -a "$AGGREGATION_METHOD" -e "$EVENT_SEQ_TYPE" -m "$MODES
echo $COMMAND
$COMMAND


## 2. run marble and analyze marble
THIS_EVENT_SEQ_FILE=$EVENT_SEQ_LABEL
THIS_EVENT_SEQ_FILE_LOCATION=$EVENT_SEQ_FILE
COMMAND="python wrapper_runTensor_analyzeResult.py -i "$SAVE_FOLDER"/data_tensor/"$THIS_EVENT_SEQ_FILE"-{0}.dat -e "$THIS_EVENT_SEQ_FILE_LOCATION" -s "$SAVE_FOLDER/results/" -R "$RANK_RANGE" -a "$ALPHA" -g "$GAMMA" -m "$EXPT_MULT_RANK
echo $COMMAND
$COMMAND


