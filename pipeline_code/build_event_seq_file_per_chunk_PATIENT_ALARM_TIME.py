## load event sequence file, chunk it up to X days

import sys
import os
import pandas as pd
import numpy as np

#path to data
path_to_data = "/homes/hny5/rchen/git/icualarm/data_csv/Alarm/"
sys.path.append(path_to_data)


#num_chunks_from_start = 14
#num_days_per_chunk = 1
#file_alarms = "../data_csv_processed/df_PATIENT_ALARM_TIMESTARTseconds_TIMEseconds_TIMEoffest_LEVEL.csv"
#label_prefix_for_new_files = "all_patients_all_alarms_day_"
#path_save_data = "event_sequence_processed/"

file_alarms = sys.argv[1]
label_prefix_for_new_files = sys.argv[2]
path_save_data = sys.argv[3]
num_chunks_from_start = int(sys.argv[4])
num_days_per_chunk = float(sys.argv[5])

df_alarms = pd.read_csv(file_alarms, names = ['PATIENT', 'ALARM', 'TIME_FIRST', 'TIME_ALARM', 'TIME_FROM_FIRST', 'LEVEL'])


seconds_per_day = 3600*24
df_alarms['CHUNK'] = np.ceil(df_alarms['TIME_FROM_FIRST']/np.float(seconds_per_day*num_days_per_chunk))


for chunk_number in range(1, num_chunks_from_start+1):
    print chunk_number
    this_chunk_save_filename = path_save_data + label_prefix_for_new_files + str(chunk_number) + ".csv"
    df_tmp = df_alarms[df_alarms['CHUNK'] == chunk_number]
    df_this_chunk = pd.DataFrame([])
    df_this_chunk['PATIENT'] = df_tmp['PATIENT']
    df_this_chunk['ALARM'] = df_tmp['ALARM']
    df_this_chunk['LEVEL'] = df_tmp['LEVEL']
    df_this_chunk.to_csv(this_chunk_save_filename, header = None, index = False)



