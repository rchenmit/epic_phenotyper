# run expSingleTensorMarble_RC.py
# run analyzeResults_RC2.py
#
# TODO 2/13: replace hard codes for model parameters!!!
#
#

import os
import sys
import json

execfile('./marble_expt_functions_RC.py')
execfile('./analyzeResults_functions.py')


#input_tensor_file = "../data_tensor/tensor_PATIENT_ALARM_COUNT_20150618-{0}.dat"
#file_raw_event_data = "../data_csv_processed/df_PATIENT_ALARM_LEVEL.csv"

'''
input_tensor_file = sys.argv[1]
file_raw_event_data = sys.argv[2]
save_results_location = sys.argv[3]
RANK = int(sys.argv[4])

#file_reason_occurrences = "../reason_mapping/d_output_reason_PERCENTPATIENTS_nf.json"
#file_med_occurrences = "../reason_mapping/d_output_med_PERCENTPATIENTS_nf.json"

alpha = sys.argv[5]
gamma = sys.argv[6]
expt_multiple = bool(sys.argv[7])
'''


## parser
import argparse
parser =  argparse.ArgumentParser(description='wrapper code to run tensor decomposition and analyze results')
parser.add_argument('--input_tensor_file', '-i', action='store', dest='input_tensor_file', 
                    type=str,
                    help='tensor input file')
parser.add_argument('--file_raw_event_data', '-e', action='store', dest='file_raw_event_data',
                    type=str,
                    help='raw event data file')
parser.add_argument('--save_results_location', '-s', action='store', dest='save_results_location', 
                    type=str,
                    help='dir to save results')
parser.add_argument('--RANK', '-r', action='store', dest='RANK',
                    type=int,
                    help='rank')
parser.add_argument('--alpha', '-a', action='store', dest='alpha',
                    type=str,
                    help='alpha')
parser.add_argument('--gamma', '-g', action='store', dest='gamma',
                    type=str,
                    help='gamma')
parser.add_argument('--RANK_RANGE', '-R', action='store', dest='RANK_RANGE', 
                    type=int, default = None, nargs='+',
                    help='rank range')
parser.add_argument('--expt_multiple', '-m', action='store', dest='expt_multiple',
                    type=int, default=0,
                    help='boolean value for whether or not we are looking for a range of RANK')
args=parser.parse_args()

input_tensor_file = args.input_tensor_file
file_raw_event_data = args.file_raw_event_data
save_results_location = args.save_results_location
alpha = args.alpha
gamma = args.gamma
RANK_RANGE = args.RANK_RANGE
RANK_LOW = RANK_RANGE[0]
RANK_HIGH = RANK_RANGE[1]
expt_multiple = bool(args.expt_multiple)
if expt_multiple:
    if len(RANK_RANGE) != 2:
        sys.exit('invalid range for RANK_RANGE; should have two values only')
    elif RANK_RANGE[1] < RANK_RANGE[0]:
        sys.exit('invalid range for RANK_RANGE')
else:
    RANK = args.RANK
    if RANK != RANK:
        sys.exit('RANK for decomposition (single rank) not specified')


#file_reason_occurrences = "../reason_mapping/d_output_reason_PERCENTPATIENTS_nf.json"
#file_med_occurrences = "../reason_mapping/d_output_med_PERCENTPATIENTS_nf.json"

input_alpha = np.float(alpha)
input_gamma = gamma.split(',')
input_gamma = [np.float(x) for x in input_gamma]

output_prefix = input_tensor_file.split("-{0}.dat")[0].split("data_tensor/")[1] 


## hard code these dictionaries for now ##NOTE: get rid of these two!
d_reason_occurrences_percent = dict()
d_med_occurrence_percent = dict()

## string for indicating gammas used ;
gamma_str = '_alpha_' + str(input_alpha) + '_gamma'
for g in input_gamma:
    gamma_str = gamma_str + '_'
    gamma_str = gamma_str + str(g)


## run expt
results_NTF_files_folder = save_results_location + "NTF_files/" + output_prefix + gamma_str + "/"

''' ##DEBUG
'''
if not os.path.exists(results_NTF_files_folder):
    os.mkdir(results_NTF_files_folder)

if expt_multiple==1:
    print "multiple"
    run_expt_multiple(input_tensor_file, results_NTF_files_folder,  output_prefix, RANK_LOW, RANK_HIGH, alpha=input_alpha, gamma=input_gamma, num_iterations=1)
else:
    print "one rank"
    run_expt(input_tensor_file, results_NTF_files_folder,  output_prefix, RANK, alpha=input_alpha, gamma=input_gamma, num_iterations=1)
''' ##END DEBUG
'''

## analyze results, make xlsx files

file_results_json = results_NTF_files_folder + output_prefix + gamma_str + ".json"
file_results_output_prefix = results_NTF_files_folder +  output_prefix + gamma_str
folder_xlsx_file_save = save_results_location + "xlsx_" + output_prefix + gamma_str + "/"
file_xlsx_file_save_prefix = folder_xlsx_file_save + output_prefix + gamma_str

print "will save results to: " + folder_xlsx_file_save
if not os.path.exists(folder_xlsx_file_save):
    os.mkdir(folder_xlsx_file_save)


#for i in range(RANK,RANK+1):
if expt_multiple:
    for i in range(RANK_LOW, RANK_HIGH+1):
        analyzeMarble(file_results_json, file_results_output_prefix + '-{0}.dat', file_xlsx_file_save_prefix + '_R_' + str(i) + '.xlsx', R = i, d_reason_occurrence_percent = d_reason_occurrences_percent, d_med_occurrence_percent = d_med_occurrence_percent, file_raw_event_data = file_raw_event_data)
else:
    for i in range(RANK, RANK+1):
        analyzeMarble(file_results_json, file_results_output_prefix + '-{0}.dat', file_xlsx_file_save_prefix + '_R_' + str(i) + '.xlsx', R = i, d_reason_occurrence_percent = d_reason_occurrences_percent, d_med_occurrence_percent = d_med_occurrence_percent, file_raw_event_data = file_raw_event_data)


