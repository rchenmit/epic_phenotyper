from collections import defaultdict
import json
import numpy as np
import pandas as pd

from openpyxl import Workbook
from openpyxl import cell
from openpyxl.styles import Font, Color, Style

import sys
sys.path.append("../tensor/")

import tensorIO
import ktensor
import tensorTools
#import SP_NTF
import marbleAPR
import marble

PLAIN_STYLE = Style(font = Font())

def reverseAxis(aDict):
    revAxis = {}
    for k, v in aDict.iteritems():
        ## reverse the values
        revAxis[k] = dict(zip(v.values(), v.keys()))
    return revAxis

def getSymptomStatistics(df, patIds, symptomName):
    ## subset the frame to contain only the patients
    tempDF = df[df.PatientID.isin(patIds.keys())]
    ## then of the patients subset to the symptom category
    tempDF = tempDF[tempDF['symptom'] == symptomName]
    ## drop any duplicates of the patient id so you're down to 1
    tempDF = tempDF.drop_duplicates(subset=["PatientID"])
    ## then we want to merge weights in
    df1 = tempDF[['PatientID', 'value']]
    df2 = pd.DataFrame(list(patIds.iteritems()), columns=['PatientID','weight'])
    tempDF = pd.merge(df1, df2, on='PatientID')
    return np.average(tempDF.value, weights=tempDF.weight), np.median(tempDF.value)

def getPatientInfo(patFactor, patDict):
    print patFactor[0:100] #debug
    patIdx = np.flatnonzero(patFactor)
#    print patIdx #debug

    nPat = len(patIdx)
    patPercent = float(nPat) / len(patFactor) * 100
    patPercent = round(patPercent, 2)
    patIds = [int(float(patDict[k])) for k in patIdx]
    return dict(zip(patIds, patFactor[patIdx])), nPat, patPercent

def getMedInfo(df, patIds, medName):
    ## subset the frame to contain only the patients
    tempDF = df[df.PatientID.isin(patIds.keys())]
    ## then of the patients subset to the symptom category
    tempDF = tempDF[tempDF.med_class == medName]
    tempDF = tempDF.drop_duplicates(subset=["PatientID", "med_v2"])
    df1 = tempDF.groupby("med_v2").agg([len])

def getModeInfo(df, patIds, modeNumber, modeElementName):
    ## subset the dataframe to contain only the patients
    tempDF = df[df[0].isin(patIds.keys())]
    ## then, subset of the patients subset to the ones that HAVE the mode feature!
    tempDF = tempDF[tempDF[modeNumber] == modeElementName]
    mean_val = tempDF[tempDF[modeNumber] == modeElementName][len(df.columns)-1].mean()
    #tempDF = tempDF.drop_duplicates(subset=[0, modeNumber])
    num_patients_have = len(np.unique(tempDF[0]))
    #df1 = tempDF.groupby( modeNumber ).agg([len])  ##don't need this right now
    percentPts = float('%.3f'%( num_patients_have / float(len(patIds)) ))
    return percentPts, mean_val

def writeCell(ws, col, row, text, style):
    ws.cell('%s%s'%(col, row)).value = text
    ws.cell('%s%s'%(col, row)).style = style

def writeCellAddRow(ws, col, row, text, style):
    writeCell(ws, col, row, text, style)
    return row + 1

def getFactorCharacteristics(factor, factorDixt):
    decOrder = np.argsort(factor)[::-1].tolist()
    filtDecOrder = filter(lambda x: factor[x] > 0, decOrder)
    return [factorDixt[idx] for idx in filtDecOrder]

def writeExcelFile(outFile, decomp, revAxis, dict_med_occurrence_percent,  dict_reason_occurrence_percent, file_raw_event_data):
    ## load the raw file
#    df = pd.read_csv(file_raw_event_data,sep=',', names = ['PatientID' , 'med', 'reason', 'reason_class', 'symptom', 'value'])

    df = pd.read_csv(file_raw_event_data, sep=',' , header = None)
    df = df[np.isfinite(df[0])] #assume first column is patient ID; only keep the "finite" i.e. "real" patients
    df[0] = df[0].astype(int) #make sure patient ID is an int
    wb = Workbook()
    ws = wb.active

    ## sort first
    decomp.normalize_sort(normtype = 1)
    R = len(decomp.lmbda)
    colA = cell.get_column_letter(1)
    colB = cell.get_column_letter(2)
    colC = cell.get_column_letter(3)
    row = 1
    cellStyles = [Style(font=Font(color=Color('FFe41a1c'))),
                    Style(font=Font(color=Color('FF377eb8'))),
                    Style(font=Font(color=Color('FF4daf4a'))),
                    Style(font=Font(color=Color('FF984ea3')))]

    num_modes = len(decomp.U) #number of modes there are; there should be AT LEAST 2 (one for patients, and one for one type of feature)
    for r in range(R):
        row = writeCellAddRow(ws, colA, row, 'Phenotype ' + str(r+1), PLAIN_STYLE)
        ## write the patient summary
        patIds, nPat, patPercent = getPatientInfo(decomp.U[0][:,r], revAxis['0'])
        writeCell(ws, colB, row, 'Percentage of patients', cellStyles[0])
        row = writeCellAddRow(ws, colA, row, 'Number of Patients: ' + str(nPat) + " (" + str(patPercent) + ")", cellStyles[0])
        
        ## for each of the modes, write them in decreasing order
        for mode_num in range(1, num_modes):
            this_mode_names = getFactorCharacteristics(decomp.U[mode_num][:,r], revAxis[str(mode_num)])
            for name in this_mode_names:
                percentPtsThisMode, meanVal = getModeInfo(df, patIds, mode_num, name)
                writeCell(ws, colA, row, name, cellStyles[mode_num])
                writeCell(ws, colB, row, percentPtsThisMode, cellStyles[mode_num])
                row = writeCellAddRow( ws, colC, row, meanVal, cellStyles[mode_num])
            ## pad it by 1 row
            row += 1
    wb.save(filename = outFile)



def analyzeMarble(runFile, dataFile, outputFile, R = 5, d_med_occurrence_percent = None, d_reason_occurrence_percent = None, file_raw_event_data = None):
    marble3D  = json.load(open(runFile, "rb"))
    revAxis = reverseAxis(marble3D['axis'])
    print np.diff(marble3D['ll'])
#    marble_decomp = marble.Marble()
#    marble_decomp.load_decomp(dataFile.format(R))
    marble_decomp = marble.Marble.load_decomp(dataFile.format(R))
    writeExcelFile(outputFile, marble_decomp[0], revAxis, d_med_occurrence_percent, d_reason_occurrence_percent, file_raw_event_data)

