from collections import defaultdict
import json
import numpy as np
import sys
sys.path.append("../tensor/")

import tensorIO
#import SP_NTF
import time
import marbleAPR

#def marbleInstance(X, r, alpha, gamma):
#    marble = SP_NTF.SP_NTF(X, r, alpha)
#    info = marble.computeDecomp(gamma=gamma)
#    lastIter = info.keys()[-1]
#    return marble, info[lastIter]['LL']

def marbleInstance(X, r, alpha, gamma):
    marble = marbleAPR.MarbleAPR(X, R=r, alpha=alpha)
    iterInfo, lastLL = marble.compute_decomp(gamma=gamma, gradual=True)
    lastIter = iterInfo.keys()[-1]
    return marble, iterInfo[lastIter]['LL']

def run_Marble(X, r, alpha, gamma, num_iterations=10):
    tenDecomp, logLL = marbleInstance(X, r, alpha, gamma)
    for i in range(1, num_iterations): # run it for set number of iterations
        xDecomp, xLL = marbleInstance(X, r, alpha, gamma)
        if xLL > logLL:
            logLL = xLL
            tenDecomp = xDecomp
    return tenDecomp, logLL    ## return the best decomposition and the LL stats

'''
def run_MarbleRange(X, alpha, gamma, RANK_LOW, RANK_HIGH):
    tenDecomp, logLL = marbleInstance(X, r, alpha, gamma)
    for r in range(RANK_LOW, RANK_HIGH):
        xDecomp, xLL = marbleInstance(X, r, alpha, gamma)
        if xLL > logLL:
            logLL = xLL
            tenDecomp = xDecomp
    return tenDecomp, logLL    ## return the best decomposition and the LL stats
'''

def decomposeFile(filename, alpha, gamma, outputName, decompName, RANK, num_iterations=10):
    X, axisDict, classDict = tensorIO.load_tensor(filename)
    fileStats = defaultdict(list)
    for r in range(int(RANK), int(RANK)+1):
        tenDecomp, logLL = run_Marble(X, r, alpha, gamma, num_iterations)
        fileStats['r'].append(r)
        fileStats['ll'].append(logLL)
        tenDecomp.saveDecomposition(decompName.format(r))
        fileStats['axis'] = axisDict
        with open(outputName, "wb") as outfile:
            json.dump(fileStats, outfile, indent=2)

def decomposeFileRange(filename, alpha, gamma, outputName, decompName, RANK_LOW, RANK_HIGH, num_iterations=10):
    X, axisDict, classDict = tensorIO.load_tensor(filename)

    fileStats = defaultdict(list)
    for r in range(RANK_LOW, RANK_HIGH +1 ):
        tenDecomp, logLL = run_Marble(X,r, alpha, gamma, num_iterations)
        fileStats['r'].append(r)
        fileStats['ll'].append(logLL)
#        tenDecomp.saveDecomposition(decompName.format(r))
        tenDecomp.save(decompName.format(r))
        fileStats['axis'] = axisDict
        with open(outputName, "wb") as outfile:
            json.dump(fileStats, outfile, indent=2)

def run_expt(input_tensor_filename, save_results_location, output_filename_prefix, RANK,  alpha=None, gamma=None, num_iterations = 10):
    print "rank="
    print RANK
    print "alpha="
    print alpha
    print "gamma="
    print gamma
    if alpha == None:
        alpha = 25000 #default
    if gamma == None:
        gamma = [0.001, 0.01, 0.05] #default
    gamma_str = '_alpha_' + str(alpha) + '_gamma'
    for g in gamma:
        gamma_str = gamma_str + '_'
        gamma_str = gamma_str + str(g)
    filename_results_json = save_results_location + output_filename_prefix + gamma_str + ".json"
    filename_results_data = save_results_location + output_filename_prefix + gamma_str + "-{0}.dat"
    if os.path.exists(filename_results_json) or os.path.exists(filename_results_data):
        sys.exit("output filename/folder already exists!" + filename_results_json + "; " + filename_results_data)
    decomposeFile(input_tensor_filename, alpha, gamma, filename_results_json , filename_results_data, RANK, num_iterations)

def run_expt_multiple(input_tensor_filename, save_results_location, output_filename_prefix, RANK_LOW, RANK_HIGH,  alpha=None, gamma=None, num_iterations = 10):
    print "rank low="
    print RANK_LOW
    print "rank high="
    print RANK_HIGH
    print "alpha="
    print alpha
    print "gamma="
    print gamma
    if alpha == None:
        alpha = 25000 #default
    if gamma == None:
        gamma = [0.001, 0.01, 0.05] #default
    gamma_str = '_alpha_' + str(alpha) + '_gamma'
    for g in gamma:
        gamma_str = gamma_str + '_'
        gamma_str = gamma_str + str(g)
       
    filename_results_json = save_results_location + output_filename_prefix + gamma_str + ".json"
    filename_results_data = save_results_location + output_filename_prefix + gamma_str + "-{0}.dat"
    if os.path.exists(filename_results_json) or os.path.exists(filename_results_data):
        sys.exit("output filename/folder already exists!" + filename_results_json + "; " + filename_results_data)
    decomposeFileRange(input_tensor_filename, alpha, gamma, filename_results_json , filename_results_data, RANK_LOW, RANK_HIGH, num_iterations)

