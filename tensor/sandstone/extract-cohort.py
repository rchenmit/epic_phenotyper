"""
File to convert the amount to amount + group
"""
import json
import sys
sys.path.append("..")

import tensorIO          # noqa

_, memMat, _, axisDict = tensorIO.load_multi_tensor("data/cms-sample12-ca-ny-tx-{0}.dat")    # noqa
cohortCost = json.load(open("data/bene12_inpatient_amt_yr3.json", "r"))

cohortDict = {}
# care about the axis dict
for i in range(memMat.shape[0]):
    for k in axisDict[i].viewkeys():
        pid = k.lower().strip()
        cohortDict[pid] = {"cost": cohortCost[pid], "group": i}

with open("data/bene12_amt_grp.json", "w") as outfile:
    json.dump(cohortDict, outfile, indent=2)
