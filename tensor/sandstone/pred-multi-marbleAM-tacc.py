import argparse
import functools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

from marbleAM import MarbleAM             # noqa
import predictionTools as pt              # noqa
import sandstonePred as spred             # noqa

INNER_ITER = 10
X = None                    # declaration to prevent errors
trainX = None               # declaration to prevent errors


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, trainX
    X, trainX = args


def run_sample(R, s, maxIter, innerIter, exptID, sample):
    ll = []
    projMat = []
    for k in range(len(X)):
        spntf = MarbleAM(trainX[k], R=R, alpha=0)
        iterInfo, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                                inner_iter=innerIter)
        print "Sample", sample, ":", lastLL
        ll.append(lastLL)
        projMat.append(spntf.project_data(X[k], 0, maxinner=INNER_ITER))
    return ll, projMat


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("cohortCost", help="cohortcost")
    parser.add_argument("eid", type=int, help="experiment id")
    parser.add_argument("rank", type=int, help="rank to evaluate")
    parser.add_argument("sample", type=int, help="sampleID")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-t", "--testSize", type=float, help="test size",
                        default=0.2)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    inputFile = args.infile
    exptID = args.eid
    testSize = args.testSize
    outerIter = args.iters
    R = args.rank
    s = args.sparsity
    dt = "multi-MarbleAM"
    ii = args.ii

    X, Y, train, test, _, _ = spred.load_multi_split(inputFile, args.cohortCost, testSize)    # noqa
    trainX = []
    trainY = []
    testY = []
    K = len(X)
    # setup the training tensor split
    for k in range(K):
        trainShape = list(X[k].shape)
        trainShape[0] = len(train[k])
        # take a subset of the tensor for training
        trainX.append(pt.subset_sptensor(X[k], train[k], trainShape))
        trainY.append(Y[k][train[k]])
        testY.append(Y[k][test[k]])

    func = functools.partial(run_sample, R, s, outerIter, ii, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, trainX))
    funcResult = pool.map_async(func, range(10)).get(9999999)
    projMat = [None for k in range(K)]
    bestLL = np.repeat(sys.float_info.max, K)
    for smpRes in funcResult:
        for k in range(K):
            # find the lowest LL and use that projection matrix
            if smpRes[0][k] < bestLL[k]:
                projMat[k] = smpRes[1][k]
                bestLL[k] = smpRes[0][k]
    predResult = [None for k in range(K)]
    for k in range(K):
        predResult[k] = spred.get_results(projMat[k][train[k], :], trainY[k],
                                          projMat[k][test[k], :], testY[k])
    print predResult
    outfileFormat = 'results/pred-{0}-{1}-{2}.json'.format(dt, exptID, args.sample)    # noqa
    RESULT = {'expt': 'pred', 'decomp': dt, 'id': exptID, "input": inputFile,
              "rank": R, "sparsity": s, "outerIters": outerIter,
              "innerIter": ii, "sample": args.sample}
    RESULT.update({"pred": predResult})
    print RESULT
    with open(outfileFormat, 'w') as outfile:
        json.dump(RESULT, outfile)


if __name__ == "__main__":
    main()
