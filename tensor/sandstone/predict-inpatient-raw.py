import argparse
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

import sandstonePred                       # noqa
import predictionTools as pt             # noqa

X = None        # get around errors
Y = None        # get around errors


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, Y
    X, Y = args


def run_set(args):
    (train, test) = args
    np.random.seed()
    trainY = Y[train]
    testY = Y[test]
    # predict using the raw features
    rawFeatures = pt.create_raw_features(X)
    rawErr = sandstonePred.get_results(rawFeatures[train, :], trainY,
                                       rawFeatures[test, :], testY)
    print rawErr
    return rawErr


def run_experiment(proc, X, targetY, ttss, resDict, outfile):
    pool = multiprocessing.Pool(processes=proc, initializer=_init,
                                initargs=(X, targetY))
    result = pool.map_async(run_set, ttss).get(9999999)
    for i, sampRes in enumerate(result):
        output = resDict
        output.update(sampRes)
        output.update({"sample": i})
        outfile.write(json.dumps(output) + "\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("cohortCost", help="cohortcost")
    parser.add_argument("eid", type=int, help="experiment id")
    parser.add_argument("-s", "--samples", type=int, default=20,
                        help="# of bootstrap samples")
    parser.add_argument("-t", "--testSize", type=float, help="test size",
                        default=0.2)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    inputFile = args.infile
    exptID = args.eid
    testSize = args.testSize
    X, Y, ttss = sandstonePred.load_split(inputFile, args.cohortCost,
                                          testSize, args.samples)
    outfile = open('results/pred-raw-{0}.json'.format(exptID), 'w')
    RESULT = {'expt': 'pred', 'decomp': 'raw', 'id': exptID,
              "input": inputFile, "R": 0}
    INPAT_RES = RESULT.copy()
    run_experiment(args.proc, X, Y, ttss, INPAT_RES, outfile)
    outfile.close()

if __name__ == "__main__":
    main()
