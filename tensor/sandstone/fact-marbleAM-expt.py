"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import functools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

from marbleAM import MarbleAM             # noqa
import tensorIO                           # noqa
import tensorTools                        # noqa


X = None                    # declaration to prevent errors
bestLL = None               # declaration to prevent errors
llLock = None               # declaration to prevent errors
decompType = "MarbleAM"     # declaration to prevent errors
JSON_FACT_OUT = "results/{0}-fact-{1}.json"   # output file format
DAT_FACT_OUT = "results/{0}-fact-{1}.dat"     # output file format


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, decompType, bestLL, llLock
    X, decompType, bestLL, llLock = args


def run_sample(R, s, maxIter, innerIter, exptID, sample):
    np.random.seed()
    spntf = MarbleAM(X, R=R, alpha=0)
    iterInfo, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                            inner_iter=innerIter)
    print "Sample", sample, ":", lastLL
    with llLock:
        if lastLL < bestLL.value:
            spntf.save(DAT_FACT_OUT.format(decompType, exptID))
            bestLL.value = lastLL
    # Analyze the overlap
    colNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=0)
    rowNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=1)
    weights = spntf.get_signal_factors().lmbda.tolist()
    return lastLL, spntf.get_signal_factors(), sample, colNNZ, rowNNZ, weights, len(iterInfo)        # noqa


def count_zeros(A):
    zeroIdx = np.where(np.asarray(A.lmbda) == 0)[0]
    return len(zeroIdx)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    R = args.r
    max_iter = args.iters
    sparsity = args.sparsity
    exptID = args.expt
    inputFile = args.infile
    dt = "MarbleAM"
    ii = args.ii

    X, axisDict, classDict = tensorIO.load_tensor(inputFile)
    factOut = open(JSON_FACT_OUT.format(dt, exptID), 'w')
    FACT_RESULT = {'expt': 'factor', 'decomp': dt, 'id': exptID, "rank": R,
                   "input": inputFile, "s": sparsity, 'maxiter': max_iter,
                   "innerIter": ii}
    bestLL = multiprocessing.Value('f', sys.float_info.max)
    llLock = multiprocessing.Lock()
    func = functools.partial(run_sample, R, sparsity, max_iter, ii, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, dt, bestLL, llLock))
    result = pool.map_async(func, range(args.samples)).get(9999999)
    for smpRes in result:
        output = FACT_RESULT
        output.update({"sample": smpRes[2], "LL": smpRes[0],
                       "colNNZ": smpRes[3], "rowNNZ": smpRes[4],
                       "weight": smpRes[5], "iters": smpRes[6]})
        factOut.write(json.dumps(output) + "\n")
    factOut.close()

if __name__ == "__main__":
    main()
