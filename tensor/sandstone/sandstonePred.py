from collections import OrderedDict
import json
import math
import numpy as np
from sklearn.cross_validation import StratifiedShuffleSplit, ShuffleSplit
from sklearn.linear_model import LassoCV
from sklearn import metrics
import sys
sys.path.append("..")

import tensorIO                 # noqa


def _train_glm(trainX, trainY, testX, testY):
    model = LassoCV(cv=20).fit(trainX, trainY)
    model.fit(trainX, trainY)
    trainMSE = metrics.mean_squared_error(trainY, model.predict(trainX))
    testMSE = metrics.mean_squared_error(testY, model.predict(testX))
    return {"train-rmse": math.sqrt(trainMSE), "test-rmse": math.sqrt(testMSE)}


def get_results(trainX, trainY, testX, testY):
    # get the normal results
    res = {}
    res['normal'] = _train_glm(trainX, trainY, testX, testY)
    res['log'] = _train_glm(trainX, np.log(trainY+1), testX, np.log(testY+1))
    return res


def load_single_split(tensorInput, cohortFile, testSize):
    # load input file
    X, axisDict, classDict = tensorIO.load_tensor(tensorInput)
    patOrder = OrderedDict(sorted(axisDict[0].items(), key=lambda t: t[1]))
    # load json with pat-cost-group
    cohortDict = json.load(open(cohortFile, "r"))
    # construct y = costs
    Y = np.array([cohortDict[k.lower().strip()]["cost"] for k in patOrder.keys()])    # noqa
    Y = Y.clip(min=0)       # clip y to make sure > 0
    # get the groupings
    grp = np.array([cohortDict[k.lower().strip()]["group"] for k in patOrder.keys()])   # noqa
    # then we want shuffled split based on regularness
    ttss = StratifiedShuffleSplit(grp, n_iter=1, test_size=testSize)
    for train, test in ttss:
        print "Get train/test split"
    return X, Y, train, test


def load_multi_split(tensorInput, cohortFile, testSize):
    # load input file
    X, memMat, modeDim, axisDict = tensorIO.load_multi_tensor(tensorInput)
    # load json with pat-cost-group
    cohortDict = json.load(open(cohortFile, "r"))
    train = []
    test = []
    Y = []
    for k in range(memMat.shape[0]):
        # get the patient order
        patOrder = OrderedDict(sorted(axisDict[k].items(), key=lambda t: t[1]))
        tmpY = np.array([cohortDict[k.lower().strip()]["cost"] for k in patOrder.keys()])   # noqa
        Y.append(tmpY.clip(min=0))  # clip and add it to the list
        # then we want shuffled split based on regularness
        print len(patOrder)
        ttss = ShuffleSplit(len(patOrder), n_iter=1, test_size=testSize)
        for tmpTrain, tmpTest in ttss:
            train.append(tmpTrain)
            test.append(tmpTest)
    return X, Y, train, test, memMat, modeDim
