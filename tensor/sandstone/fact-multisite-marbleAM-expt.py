"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import functools
import itertools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

from marbleAM import MarbleAM             # noqa
import tensorIO                           # noqa
import tensorTools                        # noqa
from ktensor import ktensor               # noqa


X = None                    # declaration to prevent errors
bestLL = None               # declaration to prevent errors
decompType = "MarbleAM"     # declaration to prevent errors
JSON_FACT_OUT = "results/multi-{0}-fact-{1}.json"   # output file format
DAT_FACT_OUT = "results/multi-{0}-{1}-fact-{2}.dat"     # output file format


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, decompType, bestLL
    X, decompType, bestLL = args


def run_sample(R, s, maxIter, innerIter, exptID, sample):
    np.random.seed()
    results = {"sample": sample}
    # keep the ktensor for each one
    decompList = []
    for tensorIdx in range(len(X)):
        spntf = MarbleAM(X[tensorIdx], R=R, alpha=0)
        iterInfo, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                                inner_iter=innerIter)
        print "(Sample, Tensor):", sample, ",", tensorIdx, lastLL
        with bestLL.get_lock():
            if lastLL < bestLL[tensorIdx]:
                spntf.save(DAT_FACT_OUT.format(tensorIdx, decompType, exptID))
                bestLL[tensorIdx] = lastLL
        # Analyze the overlap
        colNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=0)       # noqa
        rowNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=1)       # noqa
        weights = spntf.get_signal_factors().lmbda.tolist()
        tInfo = {"LL": lastLL, "colNNZ": colNNZ, "rowNNZ": rowNNZ,
                 "weight": weights, "iters": len(iterInfo)}
        results.update({"tensor_{0}".format(tensorIdx): tInfo})
        # get all but the patient mode and set weights to be 1 to ignore effect
        tmp = spntf.get_signal_factors().U
        A = [tmp[i] for i in range(1, len(tmp))]
        decompList.append(ktensor(np.repeat(1, R), A))
    # now calculate FMS similarity between pairwise
    phenoFMS = {}
    for i, j in itertools.combinations(range(len(X)), 2):
        fms = decompList[i].fms(decompList[j])
        phenoFMS.update({"({0}, {1})".format(i, j): fms})
    results.update({"phenoFMS": phenoFMS})
    return results


def count_zeros(A):
    zeroIdx = np.where(np.asarray(A.lmbda) == 0)[0]
    return len(zeroIdx)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    R = args.r
    max_iter = args.iters
    sparsity = args.sparsity
    exptID = args.expt
    inputFile = args.infile
    dt = "MarbleAM"
    ii = args.ii

    X, memMat, modeDim, axisDict = tensorIO.load_multi_tensor(inputFile)
    factOut = open(JSON_FACT_OUT.format(dt, exptID), 'w')
    FACT_RESULT = {'expt': 'factor', 'decomp': dt, 'id': exptID, "rank": R,
                   "input": inputFile, "s": sparsity, 'maxiter': max_iter,
                   "innerIter": ii}
    bestLL = multiprocessing.Array('f', np.repeat(sys.float_info.max, len(X)))
    func = functools.partial(run_sample, R, sparsity, max_iter, ii, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, dt, bestLL))
    result = pool.map_async(func, range(args.samples)).get(9999999)
    for smpRes in result:
        print smpRes
        output = FACT_RESULT
        output.update(smpRes)
        factOut.write(json.dumps(output) + "\n")
    factOut.close()

if __name__ == "__main__":
    main()
