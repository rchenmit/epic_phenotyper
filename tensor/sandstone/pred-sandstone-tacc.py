import argparse
import functools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

from sandstoneAM import SandstoneAM       # noqa
import predictionTools as pt              # noqa
import sandstonePred as spred             # noqa

INNER_ITER = 10
X = None                    # declaration to prevent errors
memMat = None               # declaration to prevent errors
modeDim = None              # declaration to prevent errors
trainX = None               # declaration to prevent errors


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, trainX, memMat, modeDim
    X, trainX, memMat, modeDim, = args


def run_sample(R, s, regL, cosReg, theta, maxIter, innerIter, exptID, sample):
    projMat = []
    print "running sample"
    ss = SandstoneAM(trainX, R, memMat, modeDim)
    _, ll = ss.compute_decomp(max_iter=maxIter, inner_iter=innerIter,
                              reg_l=regL, cos_reg=cosReg, theta=theta)
    print "finished computing decomposition"
    for k in range(len(X)):
        projMat.append(ss.project_data(X[k], inner_iter=INNER_ITER))
    return ll, projMat


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("cohortCost", help="cohortcost")
    parser.add_argument("eid", type=int, help="experiment id")
    parser.add_argument("rank", type=int, help="rank to evaluate")
    parser.add_argument("sample", type=int, help="sampleID")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-th", '--theta', nargs='+', type=float, help="theta",
                        default=None)
    parser.add_argument("-b1", '--beta1', type=float,
                        help="weight regularizer", default=0)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=0)
    parser.add_argument("-t", "--testSize", type=float, help="test size",
                        default=0.2)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    inputFile = args.infile
    exptID = args.eid
    testSize = args.testSize
    outerIter = args.iters
    R = args.rank
    s = args.sparsity
    theta = args.theta
    beta1 = args.beta1
    beta2 = args.beta2
    dt = "SandstoneAM"
    ii = args.ii

    X, Y, train, test, memMat, modeDim = spred.load_multi_split(inputFile, args.cohortCost, testSize)    # noqa
    # update mode dim
    trainX = []
    trainY = []
    testY = []
    K = len(X)
    # setup the training tensor split
    for k in range(K):
        trainShape = list(X[k].shape)
        trainShape[0] = len(train[k])
        modeDim[k] = trainShape[0]   # need to update the shape
        # take a subset of the tensor for training
        trainX.append(pt.subset_sptensor(X[k], train[k], trainShape))
        trainY.append(Y[k][train[k]])
        testY.append(Y[k][test[k]])
    print "finished setting up train/test split"
    print modeDim
    func = functools.partial(run_sample, R, s, beta1, beta2, theta,
                             outerIter, ii, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, trainX, memMat, modeDim))
    funcResult = pool.map_async(func, range(10)).get(9999999)
    projMat = [None for k in range(K)]
    bestLL = sys.float_info.max
    for smpRes in funcResult:
        if smpRes[0] < bestLL:
            projMat = smpRes[1]
            bestLL = smpRes[0]
    predResult = [None for k in range(K)]
    for k in range(K):
        predResult[k] = spred.get_results(projMat[k][train[k], :], trainY[k],
                                          projMat[k][test[k], :], testY[k])
    print predResult
    outfileFormat = 'results/pred-{0}-{1}-{2}.json'.format(dt, exptID, args.sample)    # noqa
    RESULT = {'expt': 'pred', 'decomp': dt, 'id': exptID, "input": inputFile,
              "rank": R, "sparsity": s, 'theta': theta, 'cosReg': beta2,
              "weightReg": beta1, "outerIters": outerIter, "innerIter": ii,
              "sample": args.sample}
    RESULT.update({"pred": predResult})
    with open(outfileFormat, 'w') as outfile:
        json.dump(RESULT, outfile)


if __name__ == "__main__":
    main()
