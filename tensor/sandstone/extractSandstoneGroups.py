'''
Extract results and convert it to an excel file
'''
import argparse
from openpyxl import Workbook
from openpyxl import cell
import sys
sys.path.append("..")

import tensorIO                         # noqa
import tensorTools as tt                # noqa
from sandstoneAM import SandstoneAM     # noqa
import extractResults as er             # noqa


class ExtractSandstone(er.ExtractResults):

    def write_excel(self, outFile):
        """
        Assume the phenotypes have already been sorted
        """
        wb = Workbook()
        ws = wb.active
        colA = cell.get_column_letter(1)
        colB = cell.get_column_letter(2)
        row = 1
        for r in range(self.R):
            print "Phenotype:", r
            # write the group number
            row = er.write_cell(ws, colA, row, 'Group ' + str(r + 1),
                                er.PLAIN_STYLE)
            patStr = ""
            # get the patient info for each tensor
            for pm in self.patMode:
                _, nP, pP = er.get_pat_info(self.A[pm][:, r],
                                            self.revAxis[pm])
                patStr += str(nP) + "(" + str(pP) + ");"
            row = er.write_cell(ws, colA, row, patStr, er.CELL_STYLES[0])
            for mode in tt.range_omit_k(len(self.patMode), len(self.A)):
                modeNames = er.get_factors(self.A[mode][:, r], self.revAxis[mode])       # noqa
                for elem in modeNames:
                    er.write_cell(ws, colA, row, elem[0], er.CELL_STYLES[mode])
                    row = er.write_cell(ws, colB, row, elem[1], er.CELL_STYLES[mode])    # noqa
            # pad it by 1
            row += 1
        wb.save(outFile)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("obs", help="observation tensor info")
    parser.add_argument("decomp", help="decomposition file")
    parser.add_argument("output", help="output file")
    args = parser.parse_args()
    # load the multitensor file
    X, memMat, modeDim, axisDict = tensorIO.load_multi_tensor(args.obs)
    # load the decomposition
    ss = SandstoneAM(X, 30, memMat, modeDim)
    ss.load(args.decomp)
    # get lambda and A from signal factors
    l, A = ss.get_signal_factors()
    er = ExtractSandstone(A, l, axisDict, range(ss.K))
    er.write_excel(args.output)


if __name__ == "__main__":
    main()
