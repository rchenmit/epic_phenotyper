import argparse
import numpy as np
import json
import multiprocessing
import sys
sys.path.append("..")

from marbleAM import MarbleAM             # noqa
import predictionTools as pt              # noqa
import sandstonePred                      # noqa

INNER_ITER = 10
# define these variables to avoid potential problems
X = None
Y = None
R = 1
s = None
maxIter = 0
innerIter = 10


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, Y, R, s, maxIter, innerIter    # noqa
    X, Y, R, s, maxIter, innerIter = args    # noqa


def run_set(args):
    np.random.seed()
    (train, test) = args
    # setup the new train tensor shape
    trainShape = list(X.shape)
    trainShape[0] = len(train)
    # take a subset of the tensor for training
    trainX = pt.subset_sptensor(X, train, trainShape)
    trainY = Y[train]
    testY = Y[test]
    projMat = None
    bestLL = sys.float_info.max
    for s in range(10):
        print "Running sample", s
        spntf = MarbleAM(trainX, R=R, alpha=0)
        _, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                         inner_iter=innerIter)
        if lastLL < bestLL and np.isfinite(lastLL):
            print "Projection:", INNER_ITER
            projMat = spntf.project_data(X, 0, maxinner=INNER_ITER)
            bestLL = lastLL
    return sandstonePred.get_results(projMat[train, :], trainY,
                                     projMat[test, :], testY)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("cohortCost", help="cohortcost")
    parser.add_argument("eid", type=int, help="experiment id")
    parser.add_argument("rank", type=int, help="rank to evaluate")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-t", "--testSize", type=float, help="test size",
                        default=0.2)
    parser.add_argument("-s", "--samples", type=int, default=20,
                        help="# of bootstrap samples")
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    inputFile = args.infile
    exptID = args.eid
    testSize = args.testSize
    outerIter = args.iters
    R = args.rank
    s = args.sparsity
    dt = "MarbleAM"
    ii = args.ii

    X, Y, ttss = sandstonePred.load_split(inputFile, args.cohortCost,
                                          testSize, args.samples)
    outfile = open('results/pred-{0}-{1}.json'.format(dt, exptID), 'w')
    RESULT = {'expt': 'pred', 'decomp': dt, 'id': exptID,
              "input": inputFile, "rank": R, "sparsity": s,
              "outerIters": outerIter, "innerIter": ii}
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, Y, R, s, outerIter, ii))
    result = pool.map_async(run_set, ttss).get(9999999)
    for i, sampRes in enumerate(result):
        output = RESULT.copy()
        output.update({"sample": i})
        output.update(sampRes)
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
