'''
Extract results and convert it to an excel file
'''
import argparse
import numpy as np
from openpyxl import Workbook
from openpyxl import cell
from openpyxl.styles import Font, Color, Style
import csv
import sys
sys.path.append("..")

import tensorIO                      # noqa
import tensorTools as tt             # noqa
from marbleAPR import MarbleAPR      # noqa
from graniteBCD import GraniteBCD    # noqa

# style of the font
PLAIN_STYLE = Style(font=Font())
# stephen few color scheme
CELL_STYLES = [Style(font=Font(color=Color('FF4D4D4D'))),  # gray
               Style(font=Font(color=Color('FF5DA5DA'))),  # blue
               Style(font=Font(color=Color('FFFAA43A'))),  # orange
               Style(font=Font(color=Color('FF60BD68'))),  # green
               Style(font=Font(color=Color('FFF17CB0'))),  # pink
               Style(font=Font(color=Color('FFB2912F'))),  # brown
               Style(font=Font(color=Color('FFB276B2'))),  # purple
               Style(font=Font(color=Color('FFDECF3F'))),  # yellow
               Style(font=Font(color=Color('FFF15854')))]  # red


def reverseAxis(aDict):
    """
    Reverse all the dictionaries axis
    The assumption is that both keys and values are unique
    """
    revAxis = {}
    for k, v in aDict.iteritems():
        # reverse the values
        revAxis[k] = dict(zip(v.values(), v.keys()))
    return revAxis


def write_cell(ws, col, row, text, style):
    """
    Write the cell and add a value to the row
    """
    ws.cell('%s%s' % (col, row)).value = text
    ws.cell('%s%s' % (col, row)).style = style
    return row + 1


def get_factors(factor, factorDict):
    # sort in descending order
    decOrder = np.argsort(factor)[::-1].tolist()
    # filter out non-zero values
    filtDecOrder = filter(lambda x: factor[x] > 0, decOrder)
    # return the values
    return [(factorDict[idx], round(factor[idx], 5))
            for idx in filtDecOrder]


def get_pat_info(patFactor, patDict):
    patIdx = np.flatnonzero(patFactor)
    nPat = len(patIdx)
    patPercent = float(nPat) / len(patFactor) * 100
    patPercent = round(patPercent, 2)
    patIds = [patDict[k] for k in patIdx]
    return dict(zip(patIds, patFactor[patIdx])), nPat, patPercent


class ExtractResults(object):
    patMode = 0
    A = None
    l = None
    revAxis = None
    R = 0

    def __init__(self, A, l, axisDict, patMode):
        self.patMode
        self.A = A
        self.R = A[0].shape[1]
        self.l = l
        self.revAxis = reverseAxis(axisDict)

    def write_excel(self, outFile):
        """
        Assume the phenotypes have already been sorted
        """
        wb = Workbook()
        ws = wb.active
        colA = cell.get_column_letter(1)
        colB = cell.get_column_letter(2)
        row = 1
        # sort the lambda
        rOrder = np.argsort(self.l)[::-1]
        for r in rOrder:
            print "Phenotype:", r
            row = write_cell(ws, colA, row,
                             'Group ' + str(r+1), PLAIN_STYLE)
            if not np.all(self.A[self.patMode][:, r] == 1):
                # write the patient summary
                _, nPat, patPercent = get_pat_info(self.A[self.patMode][:, r],
                                                   self.revAxis[self.patMode])
                row = write_cell(ws, colA, row,
                                 'Number of Patients: ' + str(nPat) +
                                 " (" + str(patPercent) + ")",
                                 CELL_STYLES[0])
            for mode in tt.range_omit_k(len(self.A), self.patMode):
                modeNames = get_factors(self.A[mode][:, r],
                                        self.revAxis[mode])
                for elem in modeNames:
                    write_cell(ws, colA, row, elem[0], CELL_STYLES[mode])
                    row = write_cell(ws, colB, row, elem[1], CELL_STYLES[mode])
            # pad it by 1
            row += 1
        wb.save(outFile)

    def write_csv(self, outfile):
        csvWriter = csv.writer(open(outfile, "w"))
        rOrder = np.argsort(self.l)[::-1]
        for r in rOrder:
            print "Phenotype:", r
            csvWriter.writerow(['Group ' + str(r+1)])
            # write the patient summary
            _, nPat, patPercent = get_pat_info(self.A[self.patMode][:, r],
                                               self.revAxis[self.patMode])
            csvWriter.writerow(['Number of Patients: ' + str(nPat) +
                                " (" + str(patPercent) + ")"])
            for mode in tt.range_omit_k(len(self.A), self.patMode):
                modeNames = get_factors(self.A[mode][:, r],
                                        self.revAxis[mode])
                for elem in modeNames:
                    csvWriter.writerow([elem[0], elem[1]])
            csvWriter.writerow([])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("obs", help="observation tensor info")
    parser.add_argument("decomp", help="decomposition file")
    parser.add_argument("type", help="decomposition type")
    parser.add_argument("output", help="output file")
    parser.add_argument("-pid", type=int, default=0,
                        help="patient mode index")
    args = parser.parse_args()
    # load the basis tensor file
    X, axisDict, classDict = tensorIO.load_tensor(args.obs)
    # load decomposition based on type
    if args.type == "Marble":
        decomp = MarbleAPR.load_decomp(args.decomp)
        er = ExtractResults(decomp[0].U, decomp[0].lmbda, axisDict,
                            args.pid)
        er.write_csv(args.output)
    elif args.type == "Granite":
        decomp = GraniteBCD.load_decomp(args.decomp)
        A = [decomp.U[n][:, :-1] for n in range(len(decomp.U))]
        l = decomp.lmbda[:-1]
        er = ExtractResults(A, l, axisDict, args.pid)
        er.write_excel(args.output)


if __name__ == "__main__":
    main()
