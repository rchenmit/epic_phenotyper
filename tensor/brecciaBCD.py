import numpy as np
import time
from collections import OrderedDict

from gravel import Gravel
import marbleBCD as mbd
import tensorTools as tt


_DEF_MAXITER = 1000
_DEF_DELTATOL = 1e-2
_DEF_REG_L = 0
_DEF_STEP_BETA = 0.5
_DEF_STEP_FACTOR = 1
_DEF_STEP_LAMBDA = 100
_DEF_INNERITER = 3
MAX_INF = 1e10
DIFF_TOL = 1e-10


class BrecciaBCD(Gravel):
    reg_l = 0                   # regularization parameter for weights
    innIt = 0
    s = None                    # simplex parameter
    ls_beta = 0                 # line search parameter for step decrease
    weights = None              # the running sum for each of the weights

    def __init__(self, X, R, memMat, modeDim):
        self.X = X
        self.K = memMat.shape[0]
        self.G = memMat.shape[1]
        self.R = R
        self.M = memMat
        self.Z = [None for k in range(self.K)]
        self.dim_modes = modeDim
        self.obj_k = [0 for k in range(self.K)]
        self.weights = [0 for k in range(self.K)]
        self.loss = np.repeat("poisson", self.K)

    def initialize(self, fMats=None, biasMats=None):
        self.A = tt.random_factors(self.dim_modes, self.R + 1)
        self.lmbda = [np.random.uniform(low=50, high=100, size=self.R + 1)
                      for k in range(self.K)]
        return

    def _compute_fty_A(self, gradA, t, g):
        """
        Compute F_t(A) with respect to mode g
        """
        A, check2 = mbd.compute_fty(gradA, self.A[g], t, mbd.project_mode,
                                    s=self.s[g])
        return A, check2

    def _compute_fty_Lambda(self, gradL, t, k):
        """
        F_t(y) = 1/t (y-P_c(y-t gradf(y)))
        """
        l, check2 = mbd.compute_fty(gradL, self.lmbda[k], t,
                                    mbd.project_weight)
        return l, check2

    def _calculate_W(self, n, k):
        return mbd.compute_W(self.X[k], n, self.Z[k])

    def _grad_factor_kg(self, k, g, loc, weight):
        """
        Gradient of A(g) / u(g) with respect to tensor k
        """
        othModes = self._get_negative_n(k, g)
        W = self._calculate_W((self.M[k, g]-1), k)
        return mbd.grad_kl_A(loc, weight, W, othModes)

    def _grad_F(self, g):
        """
        Calculate the gradient of the tensor factor g with respect to
        all the k matrices
        """
        tensor_k = self.get_tensor_idx(g)
        gradF = np.zeros(self.A[g].shape)
        for k in tensor_k:
            gradF += self._grad_factor_kg(k, g, self.A, self.lmbda[k])
        return gradF

    def compute_total_obj(self):
        """
        Compute the total objective
        """
        ll = super(BrecciaBCD, self).compute_total_obj()
        ll += self.reg_l * np.sum(self.weights)
        return ll

    def _grad_weight(self, k):
        """
        Calculate the grad for weight
        """
        N_k = self.get_mode_idx(k)  # get all the modes in K
        W = self._calculate_W(0, k)
        gradW = mbd.grad_weight(self.A[N_k[0]], self.A, W, self.R + 1, N_k[1:])
        gradW = np.append(np.repeat(self.reg_l, self.R), 0) + gradW
        return gradW

    def _compute_approx(self, k, x=None, xType=None, n=None):
        """
        Compute the approximation based on the type
        """
        N_k = self.get_mode_idx(k)
        # default to the original
        AHat = [self.A[j] for j in N_k]
        lHat = self.lmbda[k]
        if xType == "A" and n is not None:
            AHat[n] = x
        elif xType == "l":
            lHat = x
        return lHat, AHat

    def get_signal_factors(self):
        l = [self.lmbda[k][:-1] for k in range(self.K)]
        A = [self.A[g][:, :-1] for g in range(self.G)]
        return l, A

    def _compute_ll_k(self, rangeK, x, n, xType, update_z):
        """
        Compute the log likelihood for k
        """
        for i, k in enumerate(rangeK):
            lHat, AHat = self._compute_approx(k, x=x, xType=xType, n=n[i])
            approx = self._update_Z(k, lHat, AHat)
            if update_z:
                self.Z[k] = approx
            self.obj_k[k] = self.compute_obj_k(k, approx, lHat, AHat)

    def _update_obj_kg(self, gk, rangeK, x, n, xType, update_z=False):
        # compute the D(X(k), Z(k))
        self._compute_ll_k(rangeK, x, n, xType, update_z)
        if xType == 'l':
            for k in rangeK:
                self.weight[k] = np.sum(x[:-1])

    def _line_search(self, gradV, t, gk, tensor_k, stepFunc, ll, xType, n):
        x, check2 = stepFunc(gradV, t, gk)          # take a gradient step
        self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType)
        currentLL = self.compute_total_obj()
        prevX = x
        # head upwards
        while (currentLL - ll <= check2):
            t = t / self.ls_beta
            x, check2 = stepFunc(gradV, t, gk)
            if mbd.calculate_diff(x, prevX) < DIFF_TOL:
                break
            self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType)
            currentLL = self.compute_total_obj()
            prevX = x
        while (currentLL > ll + check2 and t > 0):
            t = self.ls_beta * t
            x, check2 = stepFunc(gradV, t, gk)
            self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType)
            currentLL = self.compute_total_obj()
        # only update if necessary
        if currentLL < ll:
            self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType,
                                update_z=True)
        return x, currentLL, t

    def update_mode_G(self, g, t, ll, gradFunc, computeFunc):
        """
        Function to do line search for mode g of the matrix
        """
        lastLL = ll
        tensor_k = self.get_tensor_idx(g)
        n = [self.M[k, g] - 1 for k in tensor_k]
        for innIt in range(self.innIt):
            gradVal = gradFunc(g)
            A, ll, t = self._line_search(gradVal, t, g, tensor_k,
                                         computeFunc, ll, xType="A", n=n)
            if t == 0:
                print "Reset t since step size is 0"
                t = _DEF_STEP_FACTOR
                # reset it back to be the old one
                self._update_obj_kg(g, tensor_k, x=self.A[g], n=n, xType="A")
                ll = lastLL
                break
            self.A[g] = A
            if (lastLL - ll < 1e-10):
                print "Minimal function change - break inner"
                break
            lastLL = ll
        return ll, t

    def update_weight_K(self, k, tInit, ll):
        """
        Function to do line search for mode g of the matrix
        """
        for innIt in range(self.innIt):
            gradVal = self._grad_weight(k)      # calculate the weight
            l_k, ll, t = self._line_search(gradVal, tInit, k, [k],
                                           self._compute_fty_Lambda,
                                           ll, xType="l", n=[None])
            self.lmbda[k] = l_k
        return ll, t

    def compute_decomp(self, **kwargs):
        # initialization parameters
        max_iters = kwargs.pop('max_iter', _DEF_MAXITER)
        delta_tol = kwargs.pop('delta_tol', _DEF_DELTATOL)
        self.ls_beta = kwargs.pop('step_beta', _DEF_STEP_BETA)
        tInit_L = kwargs.pop('step_lambda', _DEF_STEP_LAMBDA)
        tInit_FM = kwargs.pop('step_factor', _DEF_STEP_FACTOR)
        self.reg_l = kwargs.pop('l_reg', _DEF_REG_L)
        self.innIt = kwargs.pop('inner_iter', _DEF_INNERITER)
        sig = kwargs.pop('sigma_init', None)
        L = kwargs.pop('lmbda_init', None)
        self.s = np.array(kwargs.pop('sparsity', np.repeat(1, self.G)))
        self.initialize()
        if sig is not None:
            self.sigma = sig
        if L is not None:
            self.lmbda = L
        iterInfo = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        # calculate the approximation and obj for each one
        self._update_obj_kg(0, range(self.K), x=None,
                            n=np.repeat(None, self.K),
                            xType=None, update_z=True)
        self.weight = [np.sum(self.lmbda[:-1]) for k in range(self.K)]
        ll = self.compute_total_obj()
        for iteration in range(max_iters):
            startIter = time.time()
            iterLL = ll
            for g in range(self.G):
                ll = self.update_mode_G(g, tInit_FM, ll,
                                        self._grad_F,
                                        self._compute_fty_A)
            for k in range(self.K):
                ll = self.update_weight_K(k, tInit_L, ll)
            elapsed = time.time() - startIter
            iterInfo[str(iteration)] = {
                "Time": elapsed,
                "LL": ll
            }
            print "It " + str(iteration) + ":" + str(iterInfo[str(iteration)])
            if abs(iterLL - ll) < delta_tol:
                break
        return iterInfo, ll
