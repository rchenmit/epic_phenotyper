import numpy as np
import random
import math

import sys
sys.path.append("..")
from sptensor import sptensor   # noqa
import ktensor                  # noqa

MAX_TRIES = 100


def floor_precision(x):
    e = int(math.log10(x))
    tens = math.pow(10, e - 1)
    n = math.floor(x / tens)
    return n * tens


def get_min_nonzero(x):
    # filter out the zeros
    return np.min(x[np.nonzero(x)])


def find_gamma(U):
    N = len(U)
    # find min nonzero for each column
    # tmp = [np.apply_along_axis(get_min_nonzero, 0, U[n])
    #      for n in range(N)]
    gamma = [np.min(U[n][np.nonzero(U[n])]) for n in range(N)]
    return [floor_precision(gamma[n]) for n in range(N)]


def gen_solution(sz, R, AFill, LambdaHat):
    A = []
    for n in range(len(sz)):
        A.append(np.zeros((sz[n], R)))
        for r in range(R):
            # randomly select some entries to be nonzero
            nnz = random.sample(range(sz[n]), AFill[n])
            A[n][nnz, r] = np.random.random(size=AFill[n])
            # percentage of large size
            bigSamp = int(0.1 * sz[n])
            if bigSamp > AFill[n]:
                bigSamp = 1
            big = random.sample(nnz, bigSamp)
            A[n][big, r] = 100 * A[n][big, r]
    lmbda = np.random.random_integers(low=1, high=1, size=R)
    M = ktensor.ktensor(lmbda, A)
    M.normalize_sort(1)
    # generate the noise bias
    U = []
    for n in range(len(sz)):
        U.append(np.zeros((sz[n], 1)))
        U[n][:, 0] = np.random.random(size=sz[n])
    Mhat = ktensor.ktensor(np.array([1]), U)
    Mhat.normalize(1)
    Mhat.lmbda[0] = LambdaHat
    return M, Mhat


def gen_random_problem(MFull):
    # get the non-zero indices in the data
    nnz = np.nonzero(MFull._data)
    mfVals = MFull._data.flatten()
    xVals = np.reshape([np.random.poisson(l) for l in mfVals],
                       (len(mfVals), 1))
    Xsubs = np.zeros((len(mfVals), MFull.ndims()))
    Xsubs.dtype = 'int'
    for n in range(MFull.ndims()):
        Xsubs[:, n] = nnz[n]
    # filter out the zeros
    nnzIdx = np.flatnonzero(xVals)
    X = sptensor(Xsubs[nnzIdx, :], xVals[nnzIdx], MFull.shape)
    if len(nnzIdx) > 0.5 * np.array(MFull.shape).prod():
        X = X.to_dtensor()
    return X


def _random_simplex(support, nLarge):
    """
    Generate along the simplex
    """
    tmp = np.random.random(size=support)
    skew = random.sample(range(support), nLarge)
    tmp[skew] = np.random.randint(low=1, high=4) * tmp[skew]
    return tmp / np.sum(tmp)


def gen_nonoverlap_factors(M, R, support, theta):
    A = np.zeros((M, R))
    # generate the support at random
    nnz = random.sample(range(M), support)
    A[nnz, 0] = _random_simplex(support, 1)
    for r in range(1, R):
        reject = True
        tries = 0
        while reject and tries < MAX_TRIES:
            # keep trying until they're different enough
            nnz = random.sample(range(M), support)
            A[nnz, r] = _random_simplex(support, 1)
            reject = any(np.dot(A[:, x], A[:, r]) > theta for x in range(r))
            if reject:
                A[nnz, r] = np.repeat(0, support)
        if tries == MAX_TRIES - 1:
            # this means that it was rejected
            return None
    return A


def gen_nonoverlap_soln(sz, R, AFill, LambdaHat, theta, l_low=50, l_high=100):
    # make sure sz, Afill and theta are the same size
    N = len(sz)
    if not all(len(x) == N for x in [sz, AFill, theta]):
        return ValueError("The mode sizes are not the same")
    A = []
    for n in range(N):
        an = None
        while an is None:
            an = gen_nonoverlap_factors(sz[n], R, AFill[n], theta[n])
        A.append(an)
    lmbda = np.random.uniform(low=l_low, high=l_high, size=R)
    M = ktensor.ktensor(lmbda, A)
    M.normalize_sort(1)
    # generate the noise bias
    U = []
    for n in range(len(sz)):
        U.append(np.zeros((sz[n], 1)))
        U[n][:, 0] = np.random.random(size=sz[n])
    Mhat = ktensor.ktensor(np.array([1]), U)
    Mhat.normalize(1)
    Mhat.lmbda[0] = LambdaHat
    return M, Mhat
