import numpy as np
import time
from collections import OrderedDict

import khatrirao
import gravel
from gravel import Gravel
from gravel import AUG_LOCATION, REG_LOCATION, _DEF_MAXITER
import tensorTools

_DEF_MAXINNER = 10
_DEF_CONVERGE_TOL = 1e-4
_DEF_DELTATOL = 1e-2
_DEF_XICONV = 0.99


class GravelAPR(Gravel):

    def __init__(self, X, R, memMat, modeDim):
        self.obs_tensors = X
        self.num_tensors = memMat.shape[0]
        self.uniq_modes = memMat.shape[1]
        self.xsubs = [gravel._extract_tensor_nnz(lilX) for lilX in X]
        self.cp_rank = R
        self.mem_mat = memMat
        self.dim_modes = modeDim
        self.aug_min = 1.0 / (np.max(modeDim))

    def _calculate_Pi(self, k, g, aug):
        """
        Compute Pi for a particular tensor and mode
        k : tensor index
        g : mode index
        """
        ## get the nonzero mode indices
        nnz_modes = self.get_mode_indices(k)
        ## get the items
        M = [self.f_mat[aug][i] for i in nnz_modes]
        Pi = None
        X = self.obs_tensors[k]
        modes_but_n = tensorTools.range_omit_k(X.ndims(),
                                               self.mem_mat[k, g] - 1)
        if self.xsubs[k] is None:
            Pi = khatrirao.khatrirao_array([M[i] for i in modes_but_n],
                                           reverse=True)
        else:
            Pi = np.ones((X.nnz(), M[0].shape[1]))
            for nn in modes_but_n:
                Pi = np.multiply(M[nn][X.subs[:, nn], :], Pi)
        return Pi

    def _mode_absorb(self, k, g, loc):
        """
        Absorb the multipler (lambda or alpha) into factor matrix
        """
        B = self.f_mat[loc][g]
        if loc == REG_LOCATION:
            B = B * self.lambda_[np.newaxis, :]
        else:
            B = B * self.alpha[k]
        return B

    def _mode_normalize(self, tensor_k, g, loc, B):
        """
        Given a factor matrix B:
        (1) normalize the mode
        (2) update the multiplier (lambda/alpha)
        (3) update the factor
        """
        if loc == REG_LOCATION:
            BHat, B_norms = tensorTools.normalize(B, normtype=1)
            ## update lambdas to reflect the norm
            self.lambda_ = B_norms
        else:
            B = GravelAPR._adjust_aug_factors(B)
            BHat, B_norms = tensorTools.normalize(B, normtype=1)
            # for k in tensor_k:
            #     self.alpha[k] = self.alpha[k] * B_norms[0]
        return BHat

    def _compute_other_tensor(self, k, g, loc):
        """
        Calculate the value of the other tensor
        B \Psi
        """
        psi = self._calculate_Pi(k, g, loc)      # calculate pi
        B = self._mode_absorb(k, g, loc)   # absorb the multiplier
        return gravel._multiply_matrix(B, psi,
                                       self.xsubs[k][:,
                                                     self.mem_mat[k, g] - 1])

    def _solveModeSubproblem(self, tensor_k, g, augLoc,
                             maxInner, convTol, thr=0):
        """
        """
        # pre-calculate pi for each tensor
        pi = [self._calculate_Pi(k, g, augLoc) for k in tensor_k]
        # pre-compute the other tensor
        othLoc = (augLoc + 1) % 2
        ck = [self._compute_other_tensor(k, g, othLoc) for k in tensor_k]
        if augLoc == REG_LOCATION:
            B = self._mode_absorb(tensor_k[0], g, augLoc)
        else:
            ## add in multiplier to k
            B = self.f_mat[augLoc][g]
            pi = [self.alpha[k] * pi[idx] for idx, k in enumerate(tensor_k)]
        for innerI in range(maxInner):
            Phi = np.zeros((self.f_mat[augLoc][g].shape))
            ## iterate through each k
            for idx, k in enumerate(tensor_k):
                Phi += tensorTools.calculatePhi(self.obs_tensors[k],
                                                B,
                                                pi[idx],
                                                self.mem_mat[k, g] - 1,
                                                C=ck[idx])
            kktModeViolation = np.max(np.abs(np.minimum(B, 1 - Phi).flatten()))
            if (kktModeViolation < convTol):
                break
            ## mutiplicative update
            B = np.multiply(B, Phi)
        # renormalize into alpha
        B = self._mode_normalize(tensor_k, g, augLoc, B)
        B = tensorTools.hardThresholdMatrix(B, thr)
        ## update the matrix
        self.f_mat[augLoc][g] = B
        return B, (innerI + 1), kktModeViolation

    @staticmethod
    def _compute_xi(lastLL, currentLL, xi):
        xiDenom = np.max(np.absolute([lastLL, currentLL]))
        xiTemp = 1 - np.min([1, (np.absolute(lastLL - currentLL))]) / xiDenom
        if xiTemp > xi:
            ## take the mean of the two
            xi = (xi + xiTemp) / 2
        return xi

    def compute_decomp(self, **kwargs):
        # initialization parameters
        gamma = kwargs.pop('gamma', np.repeat(0, len(self.dim_modes)))
        gamma = np.array(gamma)
        max_iters = kwargs.pop('max_iter', _DEF_MAXITER)
        max_inner = kwargs.pop('max_inner', _DEF_MAXINNER)
        conv_tol = kwargs.pop('converge', _DEF_CONVERGE_TOL)
        delta_tol = kwargs.pop('delta_tol', _DEF_DELTATOL)
        xi = 0  # the gradual projection
        self.initialize(None)   # random initialization
        iterInfo = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        # pre-calculate log likelihood for each tensor
        lastLL = [self.kl_div(k) for k in range(self.mem_mat.shape[0])]
        lastLL_sum = np.sum(lastLL)  # add the sum of each value

        for iteration in range(max_iters):
            ## update hard-threshold values
            thr = xi * gamma
            ## iterate through the possible modes
            for g in range(self.uniq_modes):
                startMode = time.time()
                elapsed = time.time() - startMode
                tensor_k = self.get_tensor_indices(g)
                ## hard-threshold the mode
                self._solveModeSubproblem(tensor_k, g, REG_LOCATION, max_inner,
                                          conv_tol, thr[g])
                self._solveModeSubproblem(tensor_k, g, AUG_LOCATION, max_inner,
                                          conv_tol, 0)
                ## update the log likelihood
                for k in tensor_k:
                    lastLL[k] = self.kl_div(k)
                iterInfo[str((iteration, g))] = {
                    "Time": elapsed,
                    "KL": lastLL
                }
            ll_sum = np.sum(lastLL)
            print "Iteration: " + str(iteration) + ", KL:" + str(ll_sum)
            # calculate gradual projection update
            xi = GravelAPR._compute_xi(lastLL_sum, ll_sum, xi)
            # exit condition check
            if np.abs(lastLL_sum - ll_sum) < delta_tol and xi >= _DEF_XICONV:
                break
            lastLL_sum = ll_sum
        return iterInfo
