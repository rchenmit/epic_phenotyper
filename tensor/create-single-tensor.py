import argparse
import json
import sys
sys.path.append("../tensor")

import tensorIO as tio         # noqa


def subset_cohorts(cohortDict, patIds, negClass="0"):
    subCohort = {}
    for pid in patIds:
        if pid not in cohortDict:
            subCohort[pid] = negClass
        else:
            subCohort[pid] = cohortDict[pid]
    return subCohort


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument('outfile', help="output file")
    parser.add_argument("-sep", type=int, default=2,
                        help="delimiter seperator")
    parser.add_argument("-axis", type=int, nargs="+", default=[0, 2, 3])
    parser.add_argument("-val", type=int, default=3)
    parser.add_argument("-classFile", default=None)
    parser.add_argument("-patAxis", type=int, default=0)
    args = parser.parse_args()
    delim = ","
    if args.sep == 2:
        delim = "\t"
    X, axDict = tio.construct_tensor(args.infile, args.axis, args.val,
                                     sep=delim, axisDict=None)
    xClass = None
    if args.classFile is not None:
        print "Loading class file"
        patIds = axDict[args.patAxis].keys()
        tmp = json.load(open(args.classFile, "r"))
        xClass = subset_cohorts(tmp, patIds)
    tio.save_tensor(X, axDict, xClass, args.outfile)
    print "Tensor shape:", X.shape

if __name__ == "__main__":
    main()
