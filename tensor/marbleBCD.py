'''
Nonnegative tensor factorization for count data
Note that this uses simplex projection instead of
multiplicative update.
'''
import numpy as np
import time
from collections import OrderedDict

from marble import Marble
import tensorTools as tt
import khatrirao
from ktensor import ktensor
from dtensor import dtensor
from tenmat import tenmat
from sptensor import sptensor
from sptenmat import sptenmat
import simplex_projection as simplex
np.seterr(all='raise', under='ignore')


_DEF_GRADUAL = True
_DEF_DELTATOL = 1e-2
_DEF_CONV_TOL = 1e-4
_DEF_STEP_BETA = 0.4
_DEF_STEP_FACTOR = 1
_DEF_STEP_LAMBDA = 1
_DEF_MAXITER = 500
_DEF_INNERITER = 10
MAX_INF = 1e10
AUG_MIN = 1e-50    # an extremely small value


def grad_kl_A(U, l, W, neg_n):
    """
    The gradient of A(n) is [W]Phi^(-n)lambda
    """
    Phi = khatrirao.khatrirao_array([U[i] for i in neg_n],
                                    reverse=True)
    return np.dot(W, np.dot(Phi, np.diag(l)))


def grad_weight(U, A, W, R, neg_n):
    """
    The gradient of a weight lambda_r is:
    sum a_r^(n) a_r^(-n) W
    """
    gradL = np.zeros(R)
    for r in range(R):
        ak = khatrirao.khatrirao_array([A[n][:, [r]]
                                       for n in neg_n],
                                       reverse=True)
        ak0 = np.outer(U[:, r], ak)
        gradL[r] = gradL[r] + np.sum(np.multiply(ak0, W))
    return gradL


def compute_fty(grad_x, x, t, proj_func, **kwargs):
    """
    Compute the new value of x given gradient and step size
    and also returns 0, which is the check2 value
    """
    x_new = proj_func(x - t * grad_x, **kwargs)
    return x_new, 0


def project_weight(x):
    """
    Clip any values below 0
    """
    x = fix_A(x)
    x[:-1] = np.array(x[:-1]).clip(min=0)
    x[-1] = max(1e-10, x[-1])
    return x


def project_nonnegative_A(B, biasMin=1e-10):
    """
    Function to project membership
    """
    B = fix_A(B)
    B[:, :-1] = B[:, :-1].clip(min=0)
    B[:, -1] = np.maximum(biasMin * np.ones(B[:, -1].shape), B[:, -1])
    return B


def fix_A(A):
    """
    Fix inf to something more reasonable
    """
    A[np.isposinf(A)] = MAX_INF
    A[np.isneginf(A)] = -MAX_INF
    return A


def sanity_check_A(A):
    """
    Sanity check to make sure A is finite and ok
    """
    if not np.isfinite(A).all():
        print A
        raise ValueError("Entries of A are not finite")
    if (A < 0).any():
        print A
        raise ValueError("Entries of A are negative")
    if (A > 1).any():
        print A
        raise ValueError("Entries of A are greater than 1")


def project_mode(A, **kwargs):
    """
    Project the mode onto the feasible space.
    This converts the first R columns onto the simplex
    """
    s = kwargs.get("s", 1)          # extract simplex value
    A = fix_A(A)                    # fix floating point errors
    try:
        A[:, :-1] = np.apply_along_axis(simplex.euclidean_proj_simplex,
                                        0, A[:, :-1], s=s)
    except IndexError:
        print A
        raise ValueError("Exiting because simplex is incorrect")
    A[:, -1] = np.maximum(AUG_MIN, A[:, -1])
    A = tt.col_normalize(A)
    sanity_check_A(A)
    return A


def _matrix_norm(M):
    return pow(np.linalg.norm(M), 2)


def compute_W(X, n, Z):
    """
    Compute 1 - X_(n) / Z_(n)
    """
    if isinstance(X, dtensor):
        Xn = tenmat(X, [n])
        Zn = tenmat(Z, [n])
        E = np.ones(Xn.data.shape)
        W = E - (Xn.data / Zn.data)
    elif isinstance(X, sptensor):
        # just compute x.vals / z.vals
        try:
            wVals = X.vals.flatten() / Z
        except FloatingPointError:
            print "Move Z to interior to prevent overflow error"
            Z = Z.clip(1e-100)   # shift z off small number
            wVals = X.vals.flatten() / Z
        Xn = sptenmat(X, [n])
        W = np.ones(Xn.data.shape)
        W[Xn.subs] = W[Xn.subs] - wVals
    return W


def _log_clip(matrix, minClip=1e-150):
    return np.log(matrix.clip(min=minClip))


def _compute_sptensor_Z(X, lHat, AHat):
    xsubs = X.subs
    C = (AHat[0] * lHat[np.newaxis, :])[xsubs[:, 0], :]
    # use a log-sum-exp trick
    log_C = _log_clip(C)
    for n in range(1, len(AHat)):
        log_C = np.add(log_C, _log_clip(AHat[n][xsubs[:, n], :]))
    return np.sum(np.exp(log_C), axis=1)


def compute_Z(X, lHat, AHat):
    """
    Calculate the new approximation based on type of tensor
    """
    if isinstance(X, sptensor):
        return _compute_sptensor_Z(X, lHat, AHat)
    if isinstance(X, dtensor):
        return ktensor(lHat, AHat).to_dtensor()


def _get_np_max(X):
    return np.max(np.abs(X.flatten()))


def get_max_grad(gradVal):
    """
    Calculate the maximum gradient value
    """
    if isinstance(gradVal, list):
        maxGrad = max([lambda x: _get_np_max(x) for x in gradVal])
    else:
        maxGrad = _get_np_max(gradVal)
    return maxGrad


def _copy_A(A):
    return [A[n].copy() for n in range(len(A))]


def calculate_diff(X, prevX):
    if isinstance(X, list):
        diff = [_matrix_norm(X[n] - prevX[n]) for n in range(len(X))]
        return np.sqrt(np.sum(diff))
    else:
        return np.sqrt(_matrix_norm(X - prevX))


class MarbleBCD(Marble):
    Z = None           # the current approximation
    s = None
    othModes = None

    def __init__(self, X, R, alpha=0):
        self.obs_tensor = X
        N = X.ndims()
        self.dim_num = N
        self.cp_rank = R
        self.othModes = [tt.range_omit_k(N, n)
                         for n in range(N)]
        self.alpha = alpha

    def initialize(self, M=None):
        """
        Initialize the tensor decomposition
        """
        if M is None:
            AU = tt.random_factors(self.obs_tensor.shape, 1)
            F = tt.random_factors(self.obs_tensor.shape, self.cp_rank)
            AHat = [np.column_stack((F[n], AU[n]))
                    for n in range(self.dim_num)]
            self.cp_decomp = ktensor(np.ones(self.cp_rank + 1), AHat)
        else:
            self.cp_decomp = M

    def _calculate_W(self, n):
        """
        Calculate W(n) = [1 - X(n) / Z(n)]
        """
        return compute_W(self.obs_tensor, n, self.Z)

    def _grad_A_n(self, n):
        """
        Calculate the gradient of A(n)
        """
        W = self._calculate_W(n)
        return grad_kl_A(self.cp_decomp.U,
                         self.cp_decomp.lmbda, W,
                         self.othModes[n])

    def _grad_A(self):
        return [self._grad_A_n(n) for n in range(self.dim_num)]

    def _grad_lambda(self):
        W = self._calculate_W(0)
        return grad_weight(self.cp_decomp.U[0],
                           self.cp_decomp.U, W,
                           self.cp_rank + 1, self.othModes[0])

    def _project_factors(self, A, N):
        return [project_mode(A[n], s=self.s[n]) for n in range(N)]

    def _compute_fty_factor(self, gradB, t, proj_func):
        N = self.dim_num
        B = [self.cp_decomp.U[n] - t * gradB[n] for n in range(N)]
        B = proj_func(B, N)
        return B, 0
        #F = [(self.cp_decomp.U[n] - B[n]) for n in range(N)]
        #t1F = [np.dot(gradB[n].flatten(), F[n].flatten()) for n in range(N)]
        #t2F = np.sum([_matrix_norm(F[n]) for n in range(N)])
        #try:
        #    t2 = t2F / (2 * t)
        #    return B, t2 - np.sum(t1F)
        #except FloatingPointError:
        #    print "Dividing by small t", t
        #    return self.cp_decomp.U, 0

    def _compute_fty_A(self, gradA, t):
        A, check2 = self._compute_fty_factor(gradA, t, self._project_factors)
        l = self.cp_decomp.lmbda
        return A, l, check2

    def _compute_fty_L(self, gradL, t):
        """
        F_t(y) = 1/t (y-P_c(y-t gradf(y)))
        """
        l, check2 = compute_fty(gradL, self.cp_decomp.lmbda, t, project_weight)
        return self.cp_decomp.U, l, check2

    def compute_obj(self, Z, l, A):
        if isinstance(self.obs_tensor, sptensor):
            ll = np.sum(A[0] * l[np.newaxis, :]) - \
                np.sum(np.multiply(self.obs_tensor.vals.flatten(),
                                   np.log(Z)))
        else:
            ll = tt.gen_kl_fit(self.obs_tensor, Z)
        # sanity check that the function value is finite
        if not np.isfinite(ll):
            print A, l
            raise ValueError("Log likelihood is not finite, check A or l")      # noqa
        return ll

    def _take_step(self, gradV, t, stepFunc):
        """
        Take a gradient step and calculate all the necessary values
        """
        A, l, check2 = stepFunc(gradV, t)
        approx = compute_Z(self.obs_tensor, l, A)
        ll = self.compute_obj(approx, l, A)
        return A, l, check2, ll, approx

    def _line_search(self, gradV, t, stepBeta, stepFunc, lastLL, factor=True):
        A, l, check2, ll, approx = self._take_step(gradV, t, stepFunc)
        prevX = l.copy()
        if factor:
            prevX = _copy_A(A)
        # head upwards
        while (ll - lastLL <= check2):
            t = t / stepBeta
            A, l, check2, ll, approx = self._take_step(gradV, t, stepFunc)
            if factor:
                diff = calculate_diff(A, prevX)
                prevX = A
            else:
                diff = calculate_diff(l, prevX)
                prevX = l
            if (diff < 1e-10):
                break
        while (ll - lastLL > check2 and t > 0):
            t = stepBeta * t
            A, l, check2, ll, approx = self._take_step(gradV, t, stepFunc)
        if ll < lastLL:
            self.Z = approx   # only update if it's better
        return A, l, ll, t

    def get_signal_factors(self):
        l = self.cp_decomp.lmbda[:-1]
        A = [self.cp_decomp.U[n][:, :-1] for n in range(self.dim_num)]
        return ktensor(l, A)

    def save(self, filename):
        self.cp_decomp.save(filename)

    @staticmethod
    def load_decomp(filename):
        return ktensor.load(filename)

    def inner_iteration(self, inner_iters, lsProc, ll, t, stepBeta, tInit, convTol):          # noqa
        lastLL = ll
        for ii in range(inner_iters):
            gradVal = lsProc["grad"]()    # calculate gradient
            if (get_max_grad(gradVal) < convTol):
                break                     # gradient w/i convergence tolerance
            A, l, ll, t = self._line_search(gradVal, t, stepBeta,
                                            lsProc["func"], ll,
                                            factor=lsProc["factor"])
            if t == 0:
                print "Reset t since step size is 0"
                t = min(tInit)
                ll = lastLL
                break
            self.cp_decomp.U = A
            self.cp_decomp.lmbda = l
            print "Inner {0} ends with size {1}: {2}".format(ii, t, ll)
            if (lastLL - ll < 1e-10):
                print "minimal change in log likelihood"
                break
            lastLL = ll
        return t, ll

    def compute_decomp(self, **kwargs):
        # initialization options
        self.s = np.array(kwargs.pop('sparsity', np.repeat(1, self.dim_num)))
        M = kwargs.pop('init', None)
        max_iters = kwargs.pop('max_iter', _DEF_MAXITER)
        inner_iters = kwargs.pop('inner_iter', _DEF_INNERITER)
        delta_tol = kwargs.pop('delta_tol', _DEF_DELTATOL)
        stepBeta = kwargs.pop('step_beta', _DEF_STEP_BETA)
        # step size initialization
        tInit = [kwargs.pop('step_factor', _DEF_STEP_FACTOR),
                 kwargs.pop('step_lambda', _DEF_STEP_LAMBDA)]
        convTol = kwargs.pop('conv_tol', _DEF_CONV_TOL)
        self.initialize(M)
        self.cp_decomp.U = [project_mode(self.cp_decomp.U[n], s=self.s[n])
                            for n in range(self.dim_num)]
        self.Z = compute_Z(self.obs_tensor, self.cp_decomp.lmbda,
                           self.cp_decomp.U)
        # Dictionary to manage iteration information
        iterInfo = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        ll = self.compute_obj(self.Z, self.cp_decomp.lmbda, self.cp_decomp.U)
        lineSearchProc = [{"func": self._compute_fty_A,
                           "grad": self._grad_A,
                           "factor": True},
                          {"func": self._compute_fty_L,
                           "grad": self._grad_lambda,
                           "factor": False}]
        for iteration in range(max_iters):
            lastLL = ll
            startIter = time.time()
            for idx, lsProc in enumerate(lineSearchProc):
                print "Line search:", idx
                tInit[idx], ll = self.inner_iteration(inner_iters, lsProc, ll,
                                                      tInit[idx], stepBeta,
                                                      tInit, convTol)
            elapsed = time.time() - startIter
            diffCalc = lastLL - ll
            iterInfo[str(iteration)] = {"Time": elapsed, "LL": ll, "Diff": diffCalc}  # noqa
            print "It " + str(iteration) + ":" + str(iterInfo[str(iteration)])
            if diffCalc < delta_tol and iteration > 1:
                break
            # quick sanity check on all the matrices
            [sanity_check_A(self.cp_decomp.U[n]) for n in range(self.dim_num)]
        return iterInfo, ll

    def project_data(self, XHat, n, **kwargs):
        maxIters = kwargs.pop('max_iter', 10)
        tInit_FM = kwargs.pop('step_factor', _DEF_STEP_LAMBDA * 10)
        stepBeta = kwargs.pop('step_beta', _DEF_STEP_BETA)
        # copy things away to restore it later
        origZ = self.cp_decomp.copy()
        origX = self.obs_tensor
        self.obs_tensor = XHat
        # randomize the new U
        self.cp_decomp.U[n] = np.random.rand(XHat.shape[n],
                                             self.cp_rank + 1)
        A = _copy_A(self.cp_decomp.U)      # store off A for convenience
        self.cp_decomp.lmbda = np.ones(self.cp_rank + 1)
        self.Z = compute_Z(self.obs_tensor, self.cp_decomp.lmbda, A)
        lastLL = self.compute_obj(self.Z, self.cp_decomp.lmbda,
                                  self.cp_decomp.U)
        for iteration in range(maxIters):
            t = tInit_FM
            gradN = self._grad_A_n(n)
            A[n], check2 = compute_fty(gradN, self.cp_decomp.U[n], t,
                                       project_nonnegative_A)
            approx = compute_Z(self.obs_tensor, self.cp_decomp.lmbda, A)
            ll = self.compute_obj(approx, self.cp_decomp.lmbda, A)
            while (ll > lastLL - check2 and t > 0):
                t = stepBeta * t
                A[n], check2 = compute_fty(gradN, self.cp_decomp.U[n], t,
                                           project_nonnegative_A)
                approx = compute_Z(self.obs_tensor, self.cp_decomp.lmbda, A)
                ll = self.compute_obj(approx, self.cp_decomp.lmbda, A)
            self.Z = approx
            self.cp_decomp.U = A
            A = _copy_A(self.cp_decomp.U)
            lastLL = ll
        projMat = A[n][:, :-1]
        projMat = tt.norm_rows(projMat)
        # return everything to the original values
        self.obs_tensor = origX
        self.cp_decomp = origZ
        return projMat
