"""
Create shared tensors with
"""
import argparse
from collections import OrderedDict, defaultdict
import numpy as np
import sys
sys.path.append("../tensor")

import tensorIO as tio         # noqa


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", nargs="+", help="input file")
    parser.add_argument('-o', help="output file")
    parser.add_argument("-sep", type=int, default=2,
                        help="delimiter seperator")
    parser.add_argument("-axis", type=int, nargs="+", default=[0, 2, 3])
    parser.add_argument("-val", type=int, default=4)
    args = parser.parse_args()
    delim = ","
    if args.sep == 2:
        delim = "\t"
    K = len(args.i)
    G = len(args.axis) + K - 1
    axisDict = {}                           # main copy of the axis dictionary
    memMat = np.zeros((K, G), dtype=int)    # membership matrix
    allX = []                               # the list of tensors
    # initialize a temporary dictionary
    tmpDict = defaultdict(lambda: OrderedDict(sorted({}.items(),
                                              key=lambda t: t[1])))
    for k, infile in enumerate(args.i):
        print "Processing file:", infile
        # blow away the first axis
        tmpDict[0] = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        X, axDict = tio.construct_tensor(infile, args.axis, args.val,
                                         sep=delim, axisDict=tmpDict)
        allX.append(X)
        axisDict[k] = tmpDict[0]   # copy the patient axis
        memMat[k, k] = 1
    # now set the membership matrix accordingly
    for g in range(1, len(tmpDict)):
        axisDict[K + g - 1] = tmpDict[g]
        memMat[:, K + g - 1] = (g + 1)
    # update the size to be the largest one for consistency
    for k in range(K):
        xModes = np.flatnonzero(memMat[k, :])  # get the modes in tensor
        xShape = map(lambda x: len(axisDict[x]), xModes)  # update size
        allX[k].shape = tuple(xShape)
        print "Shape of tensor", k, ":", xShape
    tio.save_multi_tensor(allX, memMat, axisDict, args.o)


if __name__ == "__main__":
    main()
