'''
Nonnegative tensor factorization for count data
Note that this uses simplex project and absorbs weight
into the first axis and doesn't use simplex projection
'''
import numpy as np
import time
from collections import OrderedDict

import marbleBCD as mb

_DEF_GRADUAL = True
_DEF_DELTATOL = 1e-2
_DEF_CONV_TOL = 1e-4
_DEF_STEP_BETA = 0.4
_DEF_STEP_FACTOR = 1
_DEF_STEP_LAMBDA = 1
_DEF_MAXITER = 500
_DEF_INNERITER = 10
MAX_INF = 1e10
AUG_MIN = 1e-50    # an extremely small value


class MarbleAM(mb.MarbleBCD):
    Z = None           # the current approximation
    s = None           # simplex projection
    othModes = None    # other modes
    absorbMode = 0     # the mode that will absorb the weight

    def _absorb_mode(self):
        """
        Absorb the weights from lambda to mode n
        """
        self.cp_decomp.redistribute(self.absorbMode)

    def _proj_factors(self, A, n):
        return mb.project_mode(A, s=self.s[n])

    def _compute_fty_factor(self, gradA, t, proj_func):
        """
        Compute fty for non-absorbed one
        """
        A_new = [self.cp_decomp.U[n] for n in range(self.dim_num)]
        minus_am = self.othModes[self.absorbMode]
        for n in minus_am:
            A_new[n] = self.cp_decomp.U[n] - t * gradA[n]
            A_new[n] = self._proj_factors(A_new[n], n)
        return A_new, 0

    def _compute_fty_B(self, gradB, t):
        B, check2 = mb.compute_fty(gradB, self.cp_decomp.U[self.absorbMode], t,
                                   mb.project_nonnegative_A)
        A = [self.cp_decomp.U[n] for n in range(self.dim_num)]
        A[self.absorbMode] = B
        return A, self.cp_decomp.lmbda, check2

    def _grad_B(self):
        return self._grad_A_n(self.absorbMode)

    def _init_factors(self):
        for n in self.othModes[self.absorbMode]:
            self.cp_decomp.U[n] = mb.project_mode(self.cp_decomp.U[n],
                                                  s=self.s[n])

    def compute_decomp(self, **kwargs):
        """
        Compute the decomposition given the observed X
        """
        # initialization parameters
        self.s = np.array(kwargs.pop('sparsity', np.repeat(1, self.dim_num)))
        M = kwargs.pop('init', None)
        max_iters = kwargs.pop('max_iter', _DEF_MAXITER)
        inner_iters = kwargs.pop('inner_iter', _DEF_INNERITER)
        delta_tol = kwargs.pop('delta_tol', _DEF_DELTATOL)
        stepBeta = kwargs.pop('step_beta', _DEF_STEP_BETA)
        # step size initialization
        tInit = [kwargs.pop('step_A', _DEF_STEP_FACTOR),
                 kwargs.pop('step_B', _DEF_STEP_LAMBDA)]
        convTol = kwargs.pop('conv_tol', _DEF_CONV_TOL)
        self.initialize(M)
        # after initialization absorb and project
        self._absorb_mode()
        self.cp_decomp.U[self.absorbMode] = mb.project_nonnegative_A(self.cp_decomp.U[self.absorbMode])    # noqa
        self._init_factors()
        # compute the projection
        self.Z = mb.compute_Z(self.obs_tensor, self.cp_decomp.lmbda,
                              self.cp_decomp.U)
        # Dictionary to manage iteration information
        iterInfo = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        # compute the objective function
        ll = self.compute_obj(self.Z, self.cp_decomp.lmbda, self.cp_decomp.U)
        lineSearchProc = [{"func": self._compute_fty_A,
                           "grad": self._grad_A,
                           "factor": True},
                          {"func": self._compute_fty_B,
                           "grad": self._grad_B,
                           "factor": True}]
        for iteration in range(max_iters):
            lastLL = ll
            startIter = time.time()
            for idx, lsProc in enumerate(lineSearchProc):
                print "Line search:", idx
                tInit[idx], ll = self.inner_iteration(inner_iters, lsProc, ll,
                                                      tInit[idx], stepBeta,
                                                      tInit, convTol)
            elapsed = time.time() - startIter
            diffCalc = lastLL - ll
            iterInfo[str(iteration)] = {"Time": elapsed, "LL": ll, "Diff": diffCalc}  # noqa
            print "It " + str(iteration) + ":" + str(iterInfo[str(iteration)])
            if diffCalc < delta_tol and iteration > 1:
                break
            # renormalize for ease
            self.cp_decomp.normalize_mode(self.absorbMode, 1)
            # quick sanity check on all the matrices
            [mb.sanity_check_A(self.cp_decomp.U[n]) for n in range(self.dim_num)]     # noqa
        return iterInfo, ll
