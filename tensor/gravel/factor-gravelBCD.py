"""
Simultaneous tensor factorization experiment

Arguments
--------------
inputFile -    tensor input file format with {0} for the 2 separate files
expt -         the experiment number for these sets of parameters
iterations -   optional maximum number of outer iterations defaulting to 100
"""
import argparse
import functools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

import tensorIO as tio                  # noqa
import tensorTools as tt                # noqa
from gravelBCD import GravelBCD         # noqa


def _init(*args):
    """ Each pool process calls this initializer. Load the array to be populated into that process's global namespace """
    global X, bestLL, llLock
    X, bestLL, llLock = args


def run_sample(memMat, modeDim, R, s, regL, outerIter, innerIter,
               resultsDir, exptID, sample):
    np.random.seed()
    gravel = GravelBCD(X, R, memMat, modeDim)
    _, ll = gravel.compute_decomp(max_iter=outerIter, inner_iter=innerIter,
                                  l_reg=regL, sparsity=s)
    print "Sample", sample, ":", ll
    with llLock:
        if ll < bestLL.value:
            gravel.save("{0}/gravel-bcd-{1}.dat".format(resultsDir, exptID))
            bestLL.value = ll
    # Analyze the overlap
    colNNZ = [tt.count_nnz(gravel.A[mode], 0) for mode in range(memMat.shape[1])]
    rowNNZ = [tt.count_nnz(gravel.A[mode], 1) for mode in range(memMat.shape[1])]
    lmbdaNNZ = [tt.count_nnz(gravel.lmbda[k], 0) for k in range(memMat.shape[0])]
    return sample, ll, colNNZ, rowNNZ, lmbdaNNZ


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("input", help="input file to parse")
    parser.add_argument("rank", type=int)
    parser.add_argument("-i", "--iterations", type=int,
                        help="Number of outer interations", default=100)
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument("-o", help="output directory", default="results")
    parser.add_argument("-p", '--sparsity', nargs='+', type=float,
                        help="simplex specification")
    parser.add_argument("-ii", "--innerIter", type=int, default=3)
    parser.add_argument("-rl", "--reg_l", type=float, default=5)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    # experimental setup
    inputFile = args.input
    exptID = args.expt
    R = args.rank
    s = args.sparsity
    outerIter = args.iterations
    innerIter = args.innerIter
    regL = args.reg_l
    resultsDir = args.o
    # load class
    X, memMat, modeDim, axisDict = tio.load_multi_tensor(inputFile)


    outfile = open('{0}/gravel-bcd-{1}.json'.format(resultsDir, exptID), 'w')
    RESULT = {'expt': 'gravel-bcd', 'id': exptID, "rank": R,
              "input": inputFile, "s": s, 'maxiter': outerIter,
              "innerIt": innerIter, "regL": regL}
    bestLL = multiprocessing.Value('f', sys.float_info.max)  # keep track of lowest
    llLock = multiprocessing.Lock()

    func = functools.partial(run_sample, memMat, modeDim, R, s, regL, outerIter, innerIter,
                             resultsDir, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, bestLL, llLock))
    result = pool.map_async(func, range(args.samples)).get(9999999)
    for sampRes in result:
        output = RESULT
        output.update({"sample": sampRes[0], "LL": sampRes[1], "colNNZ": sampRes[2],
                      "rowNNZ": sampRes[3], "weightNNZ": sampRes[4]})
        outfile.write(json.dumps(output) + "\n")


if __name__ == "__main__":
    main()
