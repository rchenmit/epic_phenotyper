import json
import argparse
import numpy as np
import multiprocessing
import functools
import sys
sys.path.append("..")

import simGravel                    # noqa
from gravelBCD import GravelBCD     # noqa
import tensorTools as tt            # noqa

SAMPLES_PER_X = 20


def compare_factors(gravel, A, L, K):
    A_norm = [tt.col_normalize(A[g], 2) for g in range(len(A))]
    gravel_A = [tt.col_normalize(gravel.A[g][:, :-1], 2)
                for g in range(len(A))]
    # calculate raw similarity between factors
    C, rawC = tt.calc_tensor_sim(A_norm, gravel_A)
    # calculate lambda similarity
    delta = 0.01
    L_D = [np.add(L[k], delta) for k in range(len(L))]
    gravel_D = [np.add(gravel.lmbda[k][:-1], delta) for k in range(len(L))]
    rawP = [tt.calc_weight_sim(L_D[k], gravel_D[k]) for k in range(len(L))]
    P = np.prod(np.asarray(rawP), axis=0)
    # calculate the optimal indices for the shared axis
    shareG = range(K, len(gravel_A))
    shareC = np.prod(np.asarray([rawC[g] for g in shareG]), axis=0)
    origIdx, newIdx, indexes = tt.opt_index(shareC * P)
    # now that we have the optimal - calculate the other ones
    tmpScores = [rawC[g][origIdx, newIdx].tolist() for g in range(len(A))]
    # for the 0 weights you want to fix them by setting them to -1
    for k in range(K):
        zeroIdx = np.where(L[k] == 0)[0]
        # find the corresponding index in the tmp scores
        matchIdx = np.where(map(lambda x: x in zeroIdx, origIdx))[0]
        for i in matchIdx:
            tmpScores[k][i] = -1
    return tmpScores


def run_sample(MFull, R, memMat, modeDim, A, L,
               maxIter, innerIter, l_reg, sample):
    np.random.seed()
    X = simGravel.gen_random_prob(MFull)
    min_ll = None
    sim_scores = None
    for samp in range(SAMPLES_PER_X):
        gbcd = GravelBCD(X, R, memMat, modeDim)
        _, ll_samp = gbcd.compute_decomp(max_iter=maxIter,
                                         inner_iter=innerIter,
                                         reg_l=l_reg)
        print ll_samp
        if min_ll is None or ll_samp < min_ll:
            results = compare_factors(gbcd, A, L, memMat.shape[0])
            sim_scores = results
            min_ll = ll_samp
    return sample, min_ll, sim_scores


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("k", type=int, help="number of tensors")
    parser.add_argument('-md', '--modes', nargs='+', type=int,
                        help="dimension of modes",
                        default=[20, 18, 14, 15, 10])
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="non-zero entries of each mode",
                        default=[6, 5, 5, 4, 3])
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta",
                        default=[0.2, 0.2, 0.2, 0.2, 0.2])
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float,
                        help="sparsity", default=[1, 1, 1, 1, 1])
    parser.add_argument("-sc", "--sharedNumber", nargs='+', type=int,
                        default=[5, 4, 5])
    parser.add_argument("-mi", "--maxIter", type=int, default=25)
    parser.add_argument("-ii", "--innerIter", type=int, default=3)
    parser.add_argument("-rl", "--reg_l", type=float, default=5)
    parser.add_argument("-samp", "--samples", type=int, default=10)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    modeDim = args.modes
    G = len(modeDim)
    K = args.k
    R = args.r
    memMat = np.zeros((K, G), dtype=int)
    np.fill_diagonal(memMat, 1)   # set the first axis to be not-shared
    for g in range(1, G - K + 1):
        memMat[:, K + g - 1] = (g + 1)
    # generate the factors
    A, U = simGravel.gen_factors(G, R, modeDim, args.fill, args.theta)
    L = simGravel.gen_lambda(K, args.sharedNumber, R)
    Sig = simGravel.gen_sigma(K)
    MFull = simGravel.gen_orig_tensor(L, A, U, memMat, Sig)
    # setup the output file
    outfile = open('results/multisite-sim-{0}.json'.format(args.expt), 'w')
    RESULT = {'expt': 'multisite', 'id': args.expt, "rank": R,
              'modeDim': modeDim, 'memMat': memMat.tolist(),
              'sparsity': args.sparsity, 'shared_comp': args.sharedNumber,
              'innerIter': args.innerIter, 'reg_l': args.reg_l,
              'maxIter': args.maxIter, "fill": args.fill,
              'theta': args.theta
              }

    func = functools.partial(run_sample, MFull, R, memMat, modeDim, A, L,
                             args.maxIter, args.innerIter, args.reg_l)
    pool = multiprocessing.Pool(processes=args.proc)
    result = pool.map_async(func, range(args.samples)).get(9999999)
    for smpRes in result:
        output = RESULT
        output.update({"sample": smpRes[0], "scores": smpRes[2],
                       "obj": smpRes[1]})
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
