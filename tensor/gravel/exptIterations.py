import json
import argparse
import numpy as np
import sys
sys.path.append("..")

# import tensorTools
from gravelAPR import GravelAPR
import simGravel

_NUM_SAMPLES = 50


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("alpha", type=int, help="alpha")
    parser.add_argument("n", type=int, help="number of tensors")
    parser.add_argument("-mm", "--memMat", nargs='+', type=int,
                        help="memMatrix",
                        default=[1, 2, 3, 0, 1, 2, 0, 3, 1, 0, 2, 3])
    parser.add_argument('-md', '--modes', nargs='+', type=int,
                        help="dimension of modes", default=[10, 8, 6, 5])
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="non-zero entries of each mode",
                        default=[4, 3, 2, 2])
    parser.add_argument("-g", '--gamma', nargs='+', type=float, help="gamma",
                        default=[0, 0, 0, 0])
    args = parser.parse_args()

    exptID = args.expt
    R = args.r
    alpha = args.alpha
    modeDim = args.modes
    gamma = args.gamma
    AFill = args.fill
    G = len(modeDim)
    K = args.n

    memMat = args.memMat
    memMat = np.reshape(memMat, (K, G))
    print "Generating simulation data with known decomposition"
    L, A, U = simGravel.gen_solution(G, R, modeDim, AFill, 3)
    MFull = simGravel.gen_orig_tensor(L, A, U,
                                      memMat, alpha)
    for sample in range(_NUM_SAMPLES):
        ## do different problems
        X = simGravel.gen_random_prob(MFull)

        RESULT_DICT = {'expt': 'iteration', 'id': exptID, 'size': modeDim,
                       'sparsity': AFill, "rank": R, "alpha": alpha,
                       "gamma": gamma, "mem": memMat.tolist(),
                       'sample': sample}
        outfile = open('results/simulation-{0}.json'.format(exptID), 'w')
        for test in range(20):
            seed = test * 1000
            np.random.seed(seed)
            output = RESULT_DICT
            output['innerSample'] = test
            gravel = GravelAPR(X, R, memMat, modeDim)
            itInfo = gravel.compute_decomp(gamma=gamma)
            tmp = gravel.compare_factors(A, L)
            output['info'] = itInfo.popitem(last=True)
            output['kl'] = np.sum(output['info'][1]["KL"])
            output['score'] = [tmp[g].tolist() for g in range(G)]
            outfile.write(json.dumps(output) + "\n")
        outfile.close()


if __name__ == "__main__":
    main()
