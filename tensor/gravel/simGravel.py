import numpy as np
import random
import sys
sys.path.append("..")
from sptensor import sptensor   # noqa
from ktensor import ktensor     # noqa
import tensorTools              # noqa
import simultTools              # noqa


def gen_factors(N, R, nSize, aFill, nTheta):
    U = tensorTools.random_factors(nSize, 1)
    A = []
    for n in range(N):
        an = None
        while an is None:
            an = simultTools.gen_nonoverlap_factors(nSize[n], R,
                                                    aFill[n], nTheta[n])
        A.append(an)
    return A, U


def gen_lambda(K, kFill, R, l_low=500, l_high=1000):
    L = [np.random.uniform(low=l_low, high=l_high, size=R)
         for n in range(K)]
    # keep track of zeroIdx
    zeroIdx = np.zeros(R)
    for k in range(K-1):
        zIdx = random.sample(range(R), R-kFill[k])
        L[k][zIdx] = 0
        zeroIdx[zIdx] += 1
    # hacky way to fix the lambda
    lastSamp = np.arange(R)
    invalidIdx = np.where(zeroIdx == K-1)[0]
    lastSamp = np.setdiff1d(lastSamp, invalidIdx)
    zIdx = random.sample(lastSamp, R-kFill[K-1])
    L[K-1][zIdx] = 0
    return L


def gen_sigma(K, l_low=25, l_high=50):
    return [np.random.uniform(low=l_low, high=l_high, size=1)
            for n in range(K)]


def gen_orig_tensor(L, A, U, memMat, sigma):
    MFull = []
    for k in range(memMat.shape[0]):
        modeIdx = np.flatnonzero(memMat[k, :])
        lHat = np.append(L[k], sigma[k])
        AHat = [np.column_stack((A[n], U[n])) for n in modeIdx]
        M = ktensor(lHat, AHat)
        MFull.append(M.to_dtensor())
    return MFull


def gen_random_prob(MFull):
    X = []
    for M in MFull:
        # get the non-zero entries
        nnz = np.nonzero(M._data)
        mfVals = M._data.flatten()
        xVals = np.reshape([np.random.poisson(l) for l in mfVals],
                           (len(mfVals), 1))
        xSubs = np.zeros((len(mfVals), M.ndims()), dtype="int")
        for n in range(M.ndims()):
            xSubs[:, n] = nnz[n]
        # figure out which ones are non-zero and build X with it
        # to avoid extraneous properties
        nnzX = np.nonzero(xVals)
        print "Number of nonzeros:" + str(len(nnzX[0]))
        xVals = xVals[nnzX[0], :]
        xSubs = xSubs[nnzX[0], :]
        X.append(sptensor(xSubs, xVals, M.shape))
    # return the observation
    return X


def main():
    modeDim = [20, 15, 10, 5]
    memMat = np.array([[1, 2, 3, 0], [1, 2, 0, 3], [1, 0, 2, 3]])
    R = 5
    K = 3
    A, U = gen_factors(4, R, modeDim, [5, 4, 3, 3],
                       [0.4, 0.3, 0.3, 0.2])
    L = gen_lambda(K, [3, 2, 3], R)
    Sig = gen_sigma(K)
    MFull = gen_orig_tensor(L, A, U, memMat, Sig)
    gen_random_prob(MFull)


if __name__ == "__main__":
    main()
