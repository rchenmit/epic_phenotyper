"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import json
import argparse
import numpy as np
import sys
sys.path.append("..")

import simGravel
from gravelBCD import GravelBCD
import tensorTools as tt

np.seterr(all="raise")

modeDim = [20, 15, 10, 5]
memMat = np.array([[1, 2, 3, 0], [1, 2, 0, 3], [1, 0, 2, 3]])
R = 5
K = 3
A, U = simGravel.gen_factors(4, R, modeDim, [5, 4, 3, 3],
                             [0.4, 0.3, 0.3, 0.2])
L = simGravel.gen_lambda(K, [3, 2, 3], R)
Sig = simGravel.gen_sigma(K)
MFull = simGravel.gen_orig_tensor(L, A, U, memMat, Sig)

def compare_factors(gravel, A, L):
    A_norm = [tt.col_normalize(A[g], 2) for g in range(len(A))]
    gravel_A = [tt.col_normalize(gravel.A[g], 2)
                for g in range(len(A))]
    # calculate tensor sim
    C, rawC = tt.calc_tensor_sim(A_norm, gravel_A)
    rawP = [tt.calc_weight_sim(L[k], gravel.lmbda[k]) for k in range(len(L))]
    P = np.prod(np.asarray(rawP), axis=0)
    C = P * C
    origIdx, newIdx, indexes = tt.opt_index(C)
    return [rawC[g][origIdx, newIdx] for g in range(len(A))]


tmp = []
for samp in range(10):
    X = simGravel.gen_random_prob(MFull)
    gbcd = GravelBCD(X, R, memMat, modeDim)
    gbcd.compute_decomp(max_iter=150, sigma_init=Sig, lmbda_init=L)
    results = compare_factors(gbcd, A, L)
    tmp.append(results)


# def comp_sim_metrics(TM, M):
#     fms = TM.fms(M)
#     fos = TM.fos(M)
#     nnz = tensorTools.count_ktensor_nnz(M)
#     return {"fms": fms, "fos": fos, "nnz": nnz}


# def copy_decomp(M):
#     newM = {}
#     for k, v in M.iteritems():
#         newM[k] = v.copy()
#     return newM


# def run_sample(X, R, alpha, gamma, beta2, max_iter, theta):
#     decomp = None
#     ll = None
#     for count in range(10):
#         spntf = Granite(X, R=R, alpha=alpha)
#         Yinfo, lastLL = spntf.compute_decomp(gamma=gamma, beta2=beta2,
#                                              max_iter=max_iter, theta=theta,
#                                              step_lambda=200)
#         if ll is None or lastLL < ll:
#             ll = lastLL
#             decomp = copy_decomp(spntf.cp_decomp)
#     return ll, decomp


# def main():
# parser = argparse.ArgumentParser()
# parser.add_argument("expt", type=int, help="experiment number")
# parser.add_argument("r", type=int, help="rank")
# parser.add_argument("alpha", type=int, help="alpha")
# parser.add_argument('-ms', '--MS', nargs='+', type=int,
#                     help="size of matrix")
# parser.add_argument('-ll', '--LL', type=float,
#                     help="lambda low", default=100)
# parser.add_argument('-lh', '--LH', type=float,
#                     help="lambda high", default=150)
# parser.add_argument("-fl", '--fill', nargs='+', type=int,
#                     help="percentage of nonzeros")
# parser.add_argument("-g", '--gamma', nargs='+', type=float, help="theta")
# parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
# parser.add_argument("-s", '--samples', type=int,
#                     help="number of samples", default=50)
# parser.add_argument("-i", '--iters', type=int,
#                     help="number of iterations", default=500)
# parser.add_argument("-b2", '--beta2', type=float,
#                     help="beta 2", default=10)
# args = parser.parse_args()

# R = args.r
# alpha = args.alpha
# theta = args.theta
# gamma = args.gamma
# MSize = args.MS
# AFill = args.fill
# beta2 = args.beta2
# max_iter = args.iters

#  Generate the solution for comparison purposes ###

print "Generating simulation data with known decomposition"


# outfile = open('results/simulation-{0}.json'.format(args.expt), 'w')
# RESULT_DICT = {'expt': 'simulation', 'id': args.expt, 'size': MSize,
#                'sparsity': AFill, "rank": R, "alpha": alpha,
#                'beta2': beta2, 'theta': theta,
#                'lambda': TM.lmbda.tolist(),
#                "gamma": gamma, 'maxiter': max_iter}
# for sample in range(args.samples):
#     # generate a random problem
#     X = simultTools.gen_random_problem(TMFull)
#     print "Finished Generation:" + str(X.nnz())
#     ll, Z = run_sample(X, R, alpha, gamma, beta2, max_iter, theta)
#     output = RESULT_DICT
#     output.update({"sample": sample, "X-nnz": X.nnz()})
#     output.update(comp_sim_metrics(TM, Z[0]))
#     outfile.write(json.dumps(output) + "\n")
# outfile.close()

# if __name__ == "__main__":
#     main()
