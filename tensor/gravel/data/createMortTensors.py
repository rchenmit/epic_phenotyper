import numpy as np
import csv
from collections import OrderedDict
import sys
sys.path.append("../..")

import sptensor
import tensorTools


## Read the file with the class information
def readClassFile(filename, patDict, patIdx, classIdx):
	patClass = OrderedDict()
	f = open(filename, "rb")
	for row in csv.reader(f):
		if not patDict.has_key(row[patIdx]):
			print "Doesn't have: " + row[patIdx]
			continue
		patId = patDict.get(row[patIdx])
		patClass[patId] = int(row[classIdx])
	f.close()
	return patClass

physioX, physioDict = tensorTools.parse3DTensorFile("mort-physio.csv", None, None, None, 0, 1, 2, 3)
patClass = readClassFile("mort-class.csv", physioDict[0], 0, 1)
X = [physioX]
sharedModes = None
axisDict = {}
axisDict[(0,0)] = physioDict[0].keys()
axisDict[(0,1)] = physioDict[1].keys()
axisDict[(0,2)] = physioDict[2].keys()
tensorTools.saveMultiTensor(X, sharedModes, axisDict, patClass, "mort-multi-base-{0}.dat")

demoX, demoAxis = tensorTools.parseShared2DTensorFile("mort-basic.csv", axisDict[0], None, 0, 1, 2)
X = [physioX, demoX]
sharedModes = [np.array([[0,0], [1,0])]
axisDict[(1,1)] = demoAxis[1].keys()
tensorTools.saveMultiTensor(X, sharedModes, axisDict, patClass, "mort-multi-demo-{0}.dat")

medX, medAxis = tensorTools.parseShared2DTensorFile("mort-meds-count.csv", axisDict[0], None, 0, 1, 2)