"""
Experiment to evaluate the effect of the size on computation time:
(1) PCA
(2) NMF
(3) CP_APR
"""
import argparse
import json
from sklearn.decomposition import RandomizedPCA
import nimfa
import numpy as np
import sys
import time

sys.path.append("..")
import CP_APR
from sptensor import sptensor
from sptenmat import sptenmat


def perform_time(type, flatX, xprime, R, samples, iters):
    timeElapsed = []
    print("Performing Timing measurements for " + type)
    for k in range(samples):
        startTime = time.time()
        if type == "NMF":
            nmfModel = nimfa.mf(flatX, method="nmf", max_iter=iters, rank=R)
            nimfa.mf_run(nmfModel)
        elif type == "PCA":
            pcaModel = RandomizedPCA(n_components=R)
            pcaModel.fit(flatX)
        elif type == "CP_APR":
            CP_APR.cp_apr(xprime, R, maxiters=iters)
        else:
            return ValueError("Not a reasonable type specified")
        timeElapsed.append(time.time() - startTime)
    return timeElapsed


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment")
    parser.add_argument("R", help="rank", type=int)
    parser.add_argument("pat", type=int, help="number of patients")
    parser.add_argument("-m", "--iters", type=int, default=70,
                        help="the number of outer iterations")
    parser.add_argument("-i", "--infile", help="input file",
                        default='data/hf-tensor-level1-data.dat')
    parser.add_argument("-s", "--samples", help="number of samples",
                        type=int, default=10)
    args = parser.parse_args()
    # parse the input parameters
    filename = args.infile
    R = args.R
    expt = args.expt
    iters = args.iters
    samples = args.samples
    pn = args.pat
    # Load the original data
    X = sptensor.load(filename)
    ix = np.in1d(X.subs[:, 0].ravel(), np.arange(pn))
    idx = np.where(ix)[0]
    xprime = sptensor(X.subs[idx, :], X.vals[idx],
                      [pn, X.shape[1], X.shape[2]])
    # matricize along the first mode
    flatX = sptenmat(xprime, [0]).tocsrmat()
    pcaTime = perform_time("PCA", flatX, xprime, R, samples, iters)
    nmfTime = perform_time("NMF", flatX, xprime, R, samples, iters)
    ntfTime = perform_time("CP_APR", flatX, xprime, R, samples, iters)
    jsonOutput = {
        "ID": expt, "rank": R, "iterations": iters, "Pat_N": pn,
        "NMF": nmfTime, "PCA": pcaTime, "CP_APR": ntfTime
    }
    with open("results/cpu-{0}.json".format(expt), "wb") as outfile:
        json.dump(jsonOutput, outfile, indent=2)


if __name__ == "__main__":
    main()
