"""
Experiment to evaluate the effect of:
(1) gradual projection,
(2) no projection, and
(3) full projection

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
"""
import numpy as np
import json
import argparse
import time
import sys
sys.path.append("..")

from marbleAPR import MarbleAPR
import tensorTools
import simultTools
INNER_ITER = 5


def compute_result(X, TM, R, alpha, gamma, seed, gradual=True):
    np.random.seed(seed)
    startTime = time.time()
    spntf = MarbleAPR(X, R=R, alpha=alpha)
    Yinfo = spntf.compute_decomp(max_inner=INNER_ITER, gamma=gamma,
                                 gradual=gradual)
    totalTime = time.time() - startTime
    return {"gamma": gamma,
            "nnz": tensorTools.count_ktensor_nnz(spntf.cp_decomp[0]),
            "compTime": totalTime,
            "iterInfo": Yinfo,
            "fms": spntf.compare_factors(TM)}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("alpha", type=int, help="alpha")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-g", '--gamma', nargs='+', type=float, help="gamma")
    args = parser.parse_args()

    exptID = args.expt
    R = args.r
    alpha = args.alpha
    MSize = args.MS
    EXPT_GAMMA = args.gamma
    AFill = args.fill

    #  Generate the solution for comparison purposes
    print "Generating simulation data with known decomposition"
    TM, TMHat = simultTools.gen_solution(MSize, R, AFill, alpha)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    np.random.seed(0)
    outfile = open('results/projection-{0}.json'.format(exptID), 'w')

    for sample in range(10):
        # generate a random problem
        X = simultTools.gen_random_problem(TMFull)
        data = {'expt': 'projection', 'id': exptID, 'size': MSize,
                'sparsity': AFill, "rank": R, "alpha": alpha,
                "gamma": EXPT_GAMMA, 'sample': sample}
        seed = sample * 1000
        # no projection
        data["none"] = compute_result(X, TM, [0, 0, 0], seed)
        # hard projection
        data["full"] = compute_result(X, TM, EXPT_GAMMA, seed, gradual=False)
        # gradual projection
        data["gamma"] = compute_result(X, TM, EXPT_GAMMA, seed)
        # soften gamma by factor of 1e-1
        tmpG = [g * 1e-1 for g in EXPT_GAMMA]
        data["gamma2"] = compute_result(X, TM, tmpG, seed)
        # soften gamma by factor of 1e-2
        tmpG = [g * 1e-2 for g in EXPT_GAMMA]
        data["gamma1"] = compute_result(X, TM, tmpG, seed)
        outfile.write(json.dumps(data) + "\n")

    outfile.close()


if __name__ == "__main__":
    main()
