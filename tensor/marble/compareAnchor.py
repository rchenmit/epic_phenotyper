"""
Experiment to compare Marble with CP-APR

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) the size of each dimension
fl:     (optional) the non-zero percentage along each dimension
g:      (optional) the minimum non-zero entry value
"""
import json
import argparse
import numpy as np
import scipy
import sys
sys.path.append("..")

from marbleAPR import MarbleAPR
import simultTools
import tensorTools

INNER_ITER = 5
MAX_ITER = 500
_NUM_SAMPLES = 10
_DEF_ALPHA = 0


def comp_sim_metrics(TM, M):
    fms = TM.fms(M)
    fos = TM.greedy_fos(M)
    nnz = tensorTools.count_ktensor_nnz(M)
    return fms, fos, nnz


def select_anchors(TM, words=3):
    N = len(TM.U)
    binU = [scipy.sign(TM.U[n]) for n in range(N)]
    possR = range(1, TM.R)
    anchorList = []
    for r in range(words):
        modeIdx = np.random.randint(N)
        rIdx = np.random.choice(possR)
        nnzU = np.sum(binU[modeIdx], axis=1)
        valChoices = np.logical_and(nnzU == 1, binU[modeIdx][:, rIdx])
        valIdx = np.random.choice(np.where(valChoices)[0])
        anchorList.append((modeIdx, valIdx, r))
        possR.remove(rIdx)
    return anchorList


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("anchor", type=int, help="number of anchor words")
    parser.add_argument("alpha", type=int, help="alpha")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-g", '--gamma', nargs='+', type=float, help="gamma")
    args = parser.parse_args()

    exptID = args.expt
    R = args.r
    anchors = args.anchor
    alpha = args.alpha
    MSize = args.MS
    gamma = args.gamma
    AFill = args.fill

    print "Generating simulation data with known decomposition"
    ## generate the solution
    TM, TMHat = simultTools.gen_solution(MSize, R, AFill, alpha)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    ## generate an observation from the known solution
    X = simultTools.gen_random_problem(TMFull)
    anchorList = select_anchors(TM, anchors)

    RESULT_DICT = {'expt': 'anchor', 'id': exptID, 'size': MSize,
                   'sparsity': AFill, "rank": R, "alpha": alpha,
                   "gamma": gamma}

    outfile = open('results/anchor-{0}.json'.format(exptID), 'w')

    for sample in range(_NUM_SAMPLES):
        output = RESULT_DICT
        output['sample'] = sample
        seed = sample * 1000
        spntf = MarbleAPR(X, R=R, alpha=alpha)
        np.random.seed(seed)
        spntf.compute_decomp(max_inner=INNER_ITER, gamma=gamma, gradual=True,
                             max_iter=MAX_ITER, anchor=anchorList)
        fms, fos, nnz = comp_sim_metrics(TM, spntf.cp_decomp[0])
        tmp = {"fms": fms, "nnz": nnz, "anchors": True}
        output.update(tmp)
        outfile.write(json.dumps(output) + "\n")
        np.random.seed(seed)
        spntf.compute_decomp(max_inner=INNER_ITER, gamma=gamma, gradual=True,
                             max_iter=MAX_ITER)
        fms, fos, nnz = comp_sim_metrics(TM, spntf.cp_decomp[0])
        tmp = {"fms": fms, "nnz": nnz, "anchors": False}
        output.update(tmp)
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
