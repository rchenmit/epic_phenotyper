#! /bin/bash
mongo marble --eval "db.anchor.drop()"
for f in results/anchor*.json
do
    mongoimport -d marble -c anchor --type json --file $f
done 