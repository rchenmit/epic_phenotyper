import argparse
import cProfile
import pstats
import sys
sys.path.append("..")

import granite
import simultTools


def main():
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser()
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("alpha", type=int, help="alpha")
    parser.add_argument("outputFile", help="filename to store information")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument('-ll', '--LL', type=float,
                        help="lambda low", default=100)
    parser.add_argument('-lh', '--LH', type=float,
                        help="lambda high", default=150)
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-i", '--iters', type=int,
                        help="number of iterations", default=500)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=10)
    args = parser.parse_args()

    R = args.r
    alpha = args.alpha
    outputFile = args.outputFile

    #  Generate the solution for comparison purposes ###
    print "Generating simulation data with known decomposition"
    TM, TMHat = simultTools.gen_nonoverlap_soln(args.MS, R, args.fill, alpha,
                                                args.theta,
                                                args.LL, args.LH)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    gamma = simultTools.find_gamma(TM.U)
    X = simultTools.gen_random_problem(TMFull)
    gt = granite.Granite(X, R=R, alpha=alpha)
    evalStr = "gt.compute_decomp(beta2={0}, max_iter={1})"
    cProfile.runctx(evalStr.format(args.beta2, args.iters), globals(),
                    locals(), filename=outputFile)
    p = pstats.Stats(outputFile)
    p.sort_stats('cumulative').print_stats()


if __name__ == "__main__":
    main()
