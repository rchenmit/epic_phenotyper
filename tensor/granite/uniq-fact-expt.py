"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import functools
import json
import multiprocessing
import numpy as np
import itertools
import sys
sys.path.append("..")

from ktensor import ktensor               # noqa
from marbleAM import MarbleAM             # noqa
from marbleAPR import MarbleAPR           # noqa
from marbleBCD import MarbleBCD           # noqa
from tMarble import TMarbleAM             # noqa
from graniteBCD import GraniteBCD         # noqa
import tensorIO                           # noqa
import tensorTools                        # noqa


X = None                    # declaration to prevent errors
bestLL = None               # declaration to prevent errors
llLock = None               # declaration to prevent errors
decompType = "MarbleAM"     # declaration to prevent errors
JSON_UNIQ_OUT = "results/{0}-uniq-{1}.json"   # output file format
JSON_FACT_OUT = "results/{0}-fact-{1}.json"   # output file format
DAT_FACT_OUT = "results/{0}-fact-{1}.dat"     # output file format


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, decompType, bestLL, llLock
    X, decompType, bestLL, llLock = args


def run_sample(R, s, alpha, beta2, theta, k,
               maxIter, innerIter, exptID, sample):
    np.random.seed()
    if decompType == "MarbleAM":
        spntf = MarbleAM(X, R=R, alpha=0)
        iterInfo, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                         inner_iter=innerIter)
    elif decompType == "MarbleBCD":
        spntf = MarbleBCD(X, R=R, alpha=0)
        iterInfo, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                         inner_iter=innerIter)
    elif decompType == "MarbleAPR":
        spntf = MarbleAPR(X, R=R, alpha=alpha)
        iterInfo, lastLL = spntf.compute_decomp(gamma=s, max_iter=maxIter,
                                         max_inner=innerIter, gradual=True)
    elif decompType == "GraniteBCD":
        spntf = GraniteBCD(X, R=R, alpha=0)
        iterInfo, lastLL = spntf.compute_decomp(sparsity=s, cos_reg=beta2,
                                         max_iter=maxIter,
                                         inner_iter=innerIter, theta=theta)
    elif decompType == "TMarbleAM":
        spntf = TMarbleAM(X, R=R, alpha=0)
        _, lastLL = spntf.compute_decomp(sparsity=s, k=k, max_iter=maxIter,
                                         inner_iter=innerIter)
    print "Sample", sample, ":", lastLL
    with llLock:
        if lastLL < bestLL.value:
            spntf.save(DAT_FACT_OUT.format(decompType, exptID))
            bestLL.value = lastLL
    # Analyze the overlap
    colNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=0)
    rowNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=1)
    weights = spntf.get_signal_factors().lmbda.tolist()
    return lastLL, spntf.get_signal_factors(), sample, colNNZ, rowNNZ, weights, len(iterInfo)


def count_zeros(A):
    zeroIdx = np.where(np.asarray(A.lmbda) == 0)[0]
    return len(zeroIdx)


def modify_ktensor(A, removeK):
    # argsort the lmbdas
    Almbda = np.asarray(A.lmbda)
    idx = np.argsort(Almbda)[::-1]
    # remove all but the end
    lmbda = Almbda[idx[:-removeK]]
    U = [A.U[n][:, idx[:-removeK]] for n in range(len(A.U))]
    return ktensor(lmbda, U)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("type", help="decomposition type")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta",
                        default=None)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=0)
    parser.add_argument("-alpha", type=float, help="alpha", default=10)
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument('-proc', type=int, default=2)
    parser.add_argument("-k", nargs='+', type=int, help="k", default=None)
    args = parser.parse_args()

    R = args.r
    theta = args.theta
    beta2 = args.beta2
    max_iter = args.iters
    sparsity = args.sparsity
    exptID = args.expt
    inputFile = args.infile
    dt = args.type
    alpha = args.alpha
    ii = args.ii
    k = args.k

    # do a sanity check
    if dt not in ["GraniteBCD", "MarbleAM", "MarbleBCD", "MarbleAPR",
                  "TMarbleAM"]:
        raise ValueError("Unsupported decomposition")
    X, axisDict, classDict = tensorIO.load_tensor(inputFile)
    uniqOut = open(JSON_UNIQ_OUT.format(dt, exptID), 'w')
    factOut = open(JSON_FACT_OUT.format(dt, exptID), 'w')
    UNIQ_RESULT = {'expt': 'uniqueness', 'decomp': dt, 'id': exptID, "rank": R,
                   "input": inputFile, "s": sparsity, 'maxiter': max_iter,
                   "innerIter": ii}
    FACT_RESULT = {'expt': 'factor', 'decomp': dt, 'id': exptID, "rank": R,
                   "input": inputFile, "s": sparsity, 'maxiter': max_iter,
                   "innerIter": ii}
    if dt == "GraniteBCD":
        UNIQ_RESULT["theta"] = theta
        UNIQ_RESULT["cosReg"] = beta2
        FACT_RESULT["theta"] = theta
        FACT_RESULT["cosReg"] = beta2
    if dt == "TMarbleAM":
        UNIQ_RESULT["k"] = k
        FACT_RESULT["k"] = k
    elif dt == "MarbleAPR":
        UNIQ_RESULT["alpha"] = alpha
        FACT_RESULT["alpha"] = alpha
    bestLL = multiprocessing.Value('f', sys.float_info.max)
    llLock = multiprocessing.Lock()
    func = functools.partial(run_sample, R, sparsity, alpha, beta2, theta, k,
                             max_iter, ii, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, dt, bestLL, llLock))
    result = pool.map_async(func, range(args.samples)).get(9999999)
    # write the uniqueness results
    for i, j in itertools.combinations(range(args.samples), 2):
        A = result[i][1]
        B = result[j][1]
        maxZero = max(count_zeros(A), count_zeros(B))
        if maxZero > 0:
            A = modify_ktensor(A, maxZero)
            B = modify_ktensor(B, maxZero)
        fms = A.fms(B)
        fos = A.fos(B)
        output = UNIQ_RESULT
        output.update({"sample": [i, j], "fms": fms, "fos": fos})
        uniqOut.write(json.dumps(output) + "\n")
    uniqOut.close()
    for smpRes in result:
        output = FACT_RESULT
        output.update({"sample": smpRes[2], "LL": smpRes[0],
                       "colNNZ": smpRes[3], "rowNNZ": smpRes[4],
                       "weight": smpRes[5], "iters": smpRes[6]})
        factOut.write(json.dumps(output) + "\n")
    factOut.close()

if __name__ == "__main__":
    main()
