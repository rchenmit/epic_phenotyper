"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import json
import numpy as np
import sys
sys.path.append("..")

from marbleAPR import MarbleAPR     # noqa
import simultTools                  # noqa
import tensorTools                  # noqa


def comp_sim_metrics(TM, M):
    fms = TM.fms(M)
    fos = TM.fos(M)
    nnz = tensorTools.count_ktensor_nnz(M)
    return {"fms": fms, "fos": fos, "nnz": nnz}


def run_sample(MFull, TM, R, gamma, alpha, max_iter, sample):
    np.random.seed()
    X = simultTools.gen_random_problem(MFull)
    scores = None
    ll = None
    for count in range(10):
        spntf = MarbleAPR(X, R=R, alpha=alpha)
        _, lastLL = spntf.compute_decomp(gamma=gamma, max_iter=max_iter)
        if ll is None or lastLL < ll:
            ll = lastLL
            scores = comp_sim_metrics(TM, spntf.get_signal_factors())
    return sample, ll, scores


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-g", '--gamma', nargs='+', type=float, help="gamma")
    parser.add_argument("-s", '--samples', type=int,
                        help="number of samples", default=50)
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-i", '--iters', type=int,
                        help="number of iterations", default=150)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    R = args.r
    theta = args.theta
    gamma = args.gamma
    MSize = args.MS
    AFill = args.fill
    max_iter = args.iters

    #  Generate the solution for comparison purposes ###
    print "Generating simulation data with known decomposition"
    alpha = 100
    TM, TMHat = simultTools.gen_nonoverlap_soln(MSize, R, AFill, alpha, theta)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    outfile = open('results/marble-sim-{0}.json'.format(args.expt), 'w')
    RESULT_DICT = {'expt': 'marble', 'id': args.expt, 'size': MSize,
                   'fill': AFill, "rank": R, 'gamma': gamma, "alpha": alpha,
                   "theta": theta, 'lambda': TM.lmbda.tolist(),
                   'maxiter': max_iter}

    for sample in range(args.samples):
        output = RESULT_DICT
        smpRes = run_sample(TMFull, TM, R, gamma, alpha, max_iter, sample)
        output.update({"sample": smpRes[0], "scores": smpRes[2],
                       "obj": smpRes[1]})
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
