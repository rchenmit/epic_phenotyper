"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import json
import numpy as np
import sys
sys.path.append("..")

from graniteBCD import GraniteBCD         # noqa
import simultTools                  # noqa
import tensorTools                  # noqa


def comp_sim_metrics(TM, M):
    fms = TM.fms(M)
    fos = TM.fos(M)
    nnz = tensorTools.count_ktensor_nnz(M)
    return {"fms": fms, "fos": fos, "nnz": nnz}


def run_sample(MFull, TM, R, s, beta2, max_iter, theta, sample):
    np.random.seed()
    X = simultTools.gen_random_problem(MFull)
    scores = None
    ll = None
    for count in range(10):
        spntf = GraniteBCD(X, R=R)
        Yinfo, lastLL = spntf.compute_decomp(sparsity=s, cos_reg=beta2,
                                             max_iter=max_iter, theta=theta)
        if ll is None or lastLL < ll:
            ll = lastLL
            scores = comp_sim_metrics(TM, spntf.get_signal_factors())
    return sample, ll, scores


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float,
                        help="simplex project", default=[1, 1, 1])
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-s", '--samples', type=int,
                        help="number of samples", default=50)
    parser.add_argument("-i", '--iters', type=int,
                        help="number of iterations", default=150)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=10)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    R = args.r
    theta = args.theta
    s = args.sparsity
    MSize = args.MS
    AFill = args.fill
    beta2 = args.beta2
    max_iter = args.iters

    #  Generate the solution for comparison purposes ###
    print "Generating simulation data with known decomposition"
    TM, TMHat = simultTools.gen_nonoverlap_soln(MSize, R, AFill, 1000, theta,
                                                l_low=5000, l_high=10000)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    outfile = open('results/granite-sim-{0}.json'.format(args.expt), 'w')
    RESULT_DICT = {'expt': 'granite', 'id': args.expt, 'size': MSize,
                   'fill': AFill, "rank": R, 'beta2': beta2, 'theta': theta,
                   'lambda': TM.lmbda.tolist(), "s": s, 'maxiter': max_iter}
    for sample in range(args.samples):
        output = RESULT_DICT
        smpRes = run_sample(TMFull, TM, R, s, beta2, max_iter, theta, sample)
        output.update({"sample": smpRes[0], "scores": smpRes[2],
                       "obj": smpRes[1]})
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
