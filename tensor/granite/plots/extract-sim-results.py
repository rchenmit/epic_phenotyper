from pymongo import MongoClient
import numpy as np
import pandas
import argparse


def get_frame(runArr, exptType, exptID, sample, arrType, mode,
              cosReg, theta, param):
    """
    Given an array, add it with the experiment type, id, sample
    and parameter
    """
    N = len(runArr)
    mInt = int(mode)
    vals = []
    vals.extend(zip(np.repeat(exptType, N),
                    np.repeat(exptID, N),
                    np.repeat(sample, N),
                    np.repeat(arrType, N),
                    np.repeat(mode, N),
                    runArr,
                    np.repeat(cosReg, N),
                    np.repeat(theta[mInt], N),
                    np.repeat(param[mInt], N)))
    return vals


def get_expt_run(mDB, expt):
    vals = []
    for exptRun in mDB.find({"id": expt}):
        if exptRun is None:
            continue    # if you can't find it do nothing
        decompType = exptRun['expt']
        sample = exptRun['sample']
        fmsScore = exptRun["scores"]["fms"]
        nnzScore = exptRun["scores"]["nnz"]
        fosScore = exptRun["scores"]["fos"]
        cosReg = 0
        param = None
        if decompType == "granite":
            cosReg = exptRun["beta2"]
            param = exptRun["s"]
        else:
            param = exptRun["gamma"]
        theta = exptRun['theta']
        for mode in range(0, 3):
            vals.extend(get_frame(fmsScore[str(mode)], decompType, expt,
                                  sample, "fms", mode, cosReg, theta, param))
            vals.extend(get_frame(nnzScore[mode], decompType, expt,
                                  sample, "nnz", mode, cosReg, theta, param))
            vals.extend(get_frame(fosScore[str(mode)], decompType, expt,
                                  sample, "fos", mode, cosReg, theta, param))
        #vals.extend(get_frame(fmsScore["Lambda"], decompType, expt,
        #                      sample, "fms", "lambda", cosReg, theta, param))
    return vals


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('maxExpt', type=int, help="maximum index")
    parser.add_argument("output", help="output file")
    args = parser.parse_args()

    mongoC = MongoClient()
    db = mongoC.granite
    mongoDB = db["sim"]        # load the database collection
    exptRange = range(1, args.maxExpt)    # iterate through all the experiments
    totalDF = []
    for expt in exptRange:
        eNnz = get_expt_run(mongoDB, expt)
        totalDF.extend(eNnz)
    pdDF = pandas.DataFrame(totalDF, columns=["exptType", "expt", "sample",
                                              "type", "mode",
                                              "value", "cosReg", "theta", "param"])

    with open(args.output, 'w') as outfile:
        pdDF.to_csv(outfile, index=False)


if __name__ == "__main__":
    main()
