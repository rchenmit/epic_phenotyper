from pymongo import MongoClient
import pandas
import numpy as np
import argparse


def get_frame(runArr, exptType, exptID, sample, arrType, param):
    """
    Given an array, add it with the experiment type, id, sample
    and parameter
    """
    N = len(runArr)
    R = len(runArr[0])
    vals = []
    for n in range(N):
        if isinstance(param[0], list):
            vals.extend(zip(np.repeat(exptType, R),
                            np.repeat(exptID, R),
                            np.repeat(sample, R),
                            np.repeat(arrType, R),
                            np.repeat(n, R),
                            runArr[n],
                            np.repeat(param[0][n], R),
                            np.repeat(param[1][n], R),
                            np.repeat(param[2][n], R)))
        else:
            vals.extend(zip(np.repeat(exptType, R),
                            np.repeat(exptID, R),
                            np.repeat(sample, R),
                            np.repeat(arrType, R),
                            np.repeat(n, R),
                            runArr[n],
                            np.repeat(param[n], R)))
    return vals


def get_apr_expt(mDB, expt, decompType):
    vals = []
    parameter = []
    for exptRun in mDB.find({"id": expt}):
        if exptRun is None:
            continue    # if you can't find it do nothing
        sample = exptRun['sample']
        runCol = exptRun['colNNZ']
        runRow = exptRun['rowNNZ']
        exptType = exptRun['expt']

        if decompType == 'marble':
            parameter = exptRun["gamma"]
        else:
            parameter = [exptRun['s'],
                         np.repeat(exptRun['cosReg'], len(exptRun['s'])),
                         exptRun['theta']]
        print parameter
        vals.extend(get_frame(runCol, exptType, expt, sample,
                              "col-NNZ", parameter))
        vals.extend(get_frame(runRow, exptType, expt, sample,
                              "row-NNZ", parameter))
        vals.append(tuple((exptType, expt, sample, "LL", 0,
                           exptRun['LL'], 0)))
    return vals


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("collection", help="mongo collection")
    parser.add_argument("-min", type=int, default=1,
                        help="minimum index")
    parser.add_argument('maxExpt', type=int, help="maximum index")
    parser.add_argument('decomp', help="decomposition type")
    parser.add_argument("output", help="output file")
    args = parser.parse_args()

    mongoC = MongoClient()
    db = mongoC.granite
    mongoDB = db[args.collection]        # load the database collection
    exptRange = range(args.min, args.maxExpt)
    totalDF = []
    # iterate through all the experiments
    for expt in exptRange:
        eNnz = get_apr_expt(mongoDB, expt, args.decomp)
        totalDF.extend(eNnz)
    if args.decomp == "marble":
        pdDF = pandas.DataFrame(totalDF, columns=["exptType", "expt", "sample",
                                                  "type", "mode",
                                                  "value", "sparsity"])
    else:
        pdDF = pandas.DataFrame(totalDF, columns=["exptType", "expt", "sample",
                                                  "type", "mode", "value",
                                                  "sparsity", "cosReg",
                                                  "theta"])

    with open(args.output, 'w') as outfile:
        pdDF.to_csv(outfile, index=False)


if __name__ == "__main__":
    main()
