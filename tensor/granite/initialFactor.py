"""
Experiment to evaluate the initialization effect:
How does intialization impact the solution
"""
import json
import argparse
import numpy as np
import itertools
import math
import sys
sys.path.append("..")

from granite import Granite
import simultTools


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("alpha", type=int, help="alpha")
    parser.add_argument('-ms', '--MS', nargs='+', type=int,
                        help="size of matrix")
    parser.add_argument("-g", '--gamma', nargs='+', type=float, help="gamma")
    parser.add_argument('-ll', '--LL', type=float,
                        help="lambda low", default=100)
    parser.add_argument('-lh', '--LH', type=float,
                        help="lambda high", default=150)
    parser.add_argument("-fl", '--fill', nargs='+', type=int,
                        help="percentage of nonzeros")
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-s", '--samples', type=int,
                        help="number of samples", default=50)
    parser.add_argument("-i", '--iters', type=int,
                        help="number of iterations", default=500)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=10)
    args = parser.parse_args()
    R = args.r
    alpha = args.alpha
    theta = args.theta
    MSize = args.MS
    AFill = args.fill
    beta1 = args.beta1
    beta2 = args.beta2
    max_iter = args.iters
    S = args.samples

    #  Generate the solution for comparison purpose
    print "Generating simulation data with known decomposition"
    TM, TMHat = simultTools.gen_nonoverlap_soln(MSize, R, AFill, alpha, theta,
                                                args.LL, args.LH)
    TMFull = TM.to_dtensor() + TMHat.to_dtensor()
    X = simultTools.gen_random_problem(TMFull)  # generate random problem
    spntf = Granite(X, R=R, alpha=alpha)
    gamma = np.repeat(0, len(MSize))
    signalComps = []
    N = len(MSize)
    recResults = np.zeros((S, R, N + 1))
    for sample in range(S):
        spntf.compute_decomp(gamma=gamma, beta1=beta1, beta2=beta2,
                             max_iter=max_iter, theta=theta)
        # copy off the data
        signalComps.append(spntf.cp_decomp[0].copy())
        score = TM.fms(spntf.cp_decomp[0])
        for n in range(N):
            recResults[sample, :, n] = score[str(n)]
        recResults[sample, :, N] = score['Lambda']
    numCombs = math.factorial(S) / (2 * math.factorial(S - 2))
    scoreResults = np.zeros((numCombs, R, N + 1))
    k = 0
    for a, b in itertools.combinations(range(S), 2):
        score = signalComps[a].fms(signalComps[b])
        for n in range(N):
            scoreResults[k, :, n] = score[str(n)]
        scoreResults[k, :, N] = score['Lambda']
        k = k + 1
    output = {'expt': 'initial', 'id': args.expt, 'size': MSize,
              'sparsity': AFill, "rank": R, "alpha": alpha,
              'beta2': beta2, 'theta': theta,
              'lambda': TM.lmbda.tolist(),
              'maxiter': max_iter, 'scores': scoreResults.tolist(),
              'recovery': recResults.tolist()}
    with open('results/initial-{0}.json'.format(args.expt),
              'w') as outfile:
        json.dump(output, outfile, indent=2)


if __name__ == "__main__":
    main()
