"""
Experiment to evaluate the effect of the number of iterations based on
simulated data

Arguments
--------------
expt:   the unique id for this set of experiments
r:      rank of decomposition
alpha:  the weight of the bias tensor
ms:     (optional) size of each dimension
fl:     (optional) number of non-zeros along each dimension
g:      (optional) minimum non-zero entry value in solution
s:      (optional) random seed value
"""
import argparse
import functools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

from graniteBCD import GraniteBCD         # noqa
import tensorTools                        # noqa
import tensorIO                           # noqa

X = None            # declaration to prevent errors
bestLL = None       # declaration to prevent errors
llLock = None       # declaration to prevent errors


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, bestLL, llLock
    X, bestLL, llLock = args


def run_sample(R, s, beta2, theta, maxIter, resultsDir, exptID, sample):
    np.random.seed()
    spntf = GraniteBCD(X, R=R, alpha=0)
    _, lastLL = spntf.compute_decomp(sparsity=s, cos_reg=beta2,
                                     max_iter=maxIter, theta=theta)
    print "Sample", sample, ":", lastLL
    with llLock:
        if lastLL < bestLL.value and np.isfinite(lastLL):
            spntf.save("{0}/granite-bcd-{1}.dat".format(resultsDir, exptID))
            bestLL.value = lastLL
    # Analyze the overlap
    colNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=0)
    rowNNZ = tensorTools.count_ktensor_nnz(spntf.get_signal_factors(), axis=1)
    weights = spntf.get_signal_factors().lmbda.tolist()
    return sample, lastLL, colNNZ, rowNNZ, weights


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("-o", help="output directory", default="results")
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-i", '--iters', type=int,
                        help="number of iterations", default=500)
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=10)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    R = args.r
    theta = args.theta
    beta2 = args.beta2
    max_iter = args.iters
    sparsity = args.sparsity
    exptID = args.expt
    inputFile = args.infile
    resultsDir = args.o

    X, axisDict, classDict = tensorIO.load_tensor(inputFile)
    outfile = open('results/granite-fact-{0}.json'.format(exptID), 'w')
    RESULT = {'expt': 'graniteFact', 'id': exptID, "rank": R,
              "input": inputFile, "cosReg": beta2,
              'theta': theta, "s": sparsity, 'maxiter': max_iter}
    # tracker for lowest LL
    bestLL = multiprocessing.Value('f', sys.float_info.max)
    llLock = multiprocessing.Lock()
    func = functools.partial(run_sample, R, sparsity, beta2, theta, max_iter,
                             resultsDir, exptID)
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, bestLL, llLock))
    result = pool.map_async(func, range(args.samples)).get(9999999)
    for sampRes in result:
        output = RESULT
        output.update({"sample": sampRes[0], "LL": sampRes[1],
                       "colNNZ": sampRes[2], "rowNNZ": sampRes[3],
                       "weight": sampRes[4]})
        outfile.write(json.dumps(output) + "\n")
    outfile.close()

if __name__ == "__main__":
    main()
