import argparse
import json
import multiprocessing
import numpy as np
from sklearn import decomposition
import sys
sys.path.append("..")

import predictionTools as pt             # noqa
import granitePred                       # noqa

INNER_ITER = 10
X = None        # get around errors
Y = None        # get around errors
R = 1           # get around errors


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, Y, R
    X, Y, R = args


def run_set(args):
    (train, test) = args
    np.random.seed()
    trainY = Y[train]
    testY = Y[test]
    # predict using NMF on the raw features
    rawFeatures = pt.create_raw_features(X)
    model = decomposition.NMF(n_components=R)
    model.fit(rawFeatures[train, :])
    nmfFeat = model.transform(rawFeatures)
    nmfErr = granitePred.get_results(nmfFeat[train, :], trainY,
                                     nmfFeat[test, :], testY)
    return nmfErr


def run_experiment(proc, X, targetY, R, ttss, resDict, outfile):
    pool = multiprocessing.Pool(processes=proc, initializer=_init,
                                initargs=(X, targetY, R))
    result = pool.map_async(run_set, ttss).get(9999999)
    for i, sampRes in enumerate(result):
        output = resDict
        output.update(sampRes)
        output.update({"sample": i})
        outfile.write(json.dumps(output) + "\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("eid", type=int, help="experiment id")
    parser.add_argument("R", type=int, help="rank")
    parser.add_argument("-s", "--samples", type=int, default=20,
                        help="# of bootstrap samples")
    parser.add_argument("-t", "--testSize", type=float, help="test size",
                        default=0.2)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    inputFile = args.infile
    exptID = args.eid
    testSize = args.testSize
    R = args.R

    X, inpatientY, outpatientY, ttss = granitePred.load_split(inputFile,
                                                              testSize,
                                                              args.samples)
    inpatientY = inpatientY.clip(min=0)       # clip y and x
    outpatientY = outpatientY.clip(min=0)
    outfile = open('results/pred-nmf-{0}.json'.format(exptID), 'w')
    RESULT = {'expt': 'pred', 'decomp': "NMF", 'id': exptID,
              "input": inputFile, "R": R}
    INPAT_RES = RESULT.copy()
    INPAT_RES.update({"type": "inpatient"})
    run_experiment(args.proc, X, inpatientY, R, ttss, INPAT_RES, outfile)
    OUTPAT_RES = RESULT.copy()
    OUTPAT_RES.update({"type": "outpatient"})
    run_experiment(args.proc, X, outpatientY, R, ttss, OUTPAT_RES, outfile)
    outfile.close()

if __name__ == "__main__":
    main()
