#! /bin/bash
# mongo granite --eval "db.initFactor.drop()"
# for f in results/initial*.json
# do
#     mongoimport -d granite -c initFactor --type json --file $f --jsonArray
# done 

mongo granite --eval "db.sim.drop()"
for f in results/granite-sim*.json
do
    mongoimport -d granite -c sim --type json --file $f --jsonArray
done 
for f in results/marble-sim*.json
do
    echo $f
    mongoimport -d granite -c sim --type json --file $f --jsonArray
done 

mongo granite --eval "db.marble.drop()"
for f in results/marble-fact-*.json
do
    echo $f
    mongoimport -d granite -c marble --type json --file $f --jsonArray
done 

mongo granite --eval "db.granite.drop()"
for f in results/granite-fact-*.json
do
    echo $f
    mongoimport -d granite -c granite --type json --file $f --jsonArray
done 
# for f in results/mb-bcd-*.json
# do
#     mongoimport -d granite -c marble --type json --file $f --jsonArray
# done 
mongo granite --eval "db.pred.drop()"
for f in results/pred-*.json
do
    echo $f
    mongoimport -d granite -c pred --type json --file $f --jsonArray
done
