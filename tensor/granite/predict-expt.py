import argparse
import numpy as np
import json
import multiprocessing
import sys
sys.path.append("..")

from marbleAM import MarbleAM             # noqa
from marbleAPR import MarbleAPR           # noqa
from marbleBCD import MarbleBCD           # noqa
from graniteBCD import GraniteBCD         # noqa
import granitePred                         # noqa
import predictionTools as pt               # noqa

INNER_ITER = 10
# define these variables to avoid potential problems
X = None
YO = None
YI = None
decompType = "MarbleAM"
R = 1
alpha = 0
cosReg = 0
theta = None
s = None
maxIter = 0
innerIter = 10


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, YI, YO, decompType, R, alpha, cosReg, theta, s, maxIter, innerIter    # noqa
    X, YI, YO, decompType, R, alpha, cosReg, theta, s, maxIter, innerIter = args    # noqa


def run_set(args):
    np.random.seed()
    (train, test) = args
    # setup the new train tensor shape
    trainShape = list(X.shape)
    trainShape[0] = len(train)
    # take a subset of the tensor for training
    trainX = pt.subset_sptensor(X, train, trainShape)
    trainYO = YO[train]
    testYO = YO[test]
    trainYI = YI[train]
    testYI = YI[test]
    projMat = None
    bestLL = sys.float_info.max
    for s in range(10):
        print "Running sample", s
        if decompType == "MarbleAM":
            spntf = MarbleAM(trainX, R=R, alpha=0)
            _, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                             inner_iter=innerIter)
        elif decompType == "MarbleBCD":
            spntf = MarbleBCD(trainX, R=R, alpha=0)
            _, lastLL = spntf.compute_decomp(sparsity=s, max_iter=maxIter,
                                             inner_iter=innerIter)
        elif decompType == "MarbleAPR":
            spntf = MarbleAPR(trainX, R=R, alpha=alpha)
            _, lastLL = spntf.compute_decomp(gamma=s, max_iter=maxIter,
                                             max_inner=innerIter, gradual=True)
        elif decompType == "GraniteBCD":
            spntf = GraniteBCD(trainX, R=R, alpha=0)
            _, lastLL = spntf.compute_decomp(sparsity=s, cos_reg=cosReg,
                                             max_iter=maxIter,
                                             inner_iter=innerIter, theta=theta)
        if lastLL < bestLL and np.isfinite(lastLL):
            print "Projection:", INNER_ITER
            projMat = spntf.project_data(X, 0, maxinner=INNER_ITER)
            bestLL = lastLL
    yo = granitePred.get_results(projMat[train, :], trainYO,
                                 projMat[test, :], testYO)
    yi = granitePred.get_results(projMat[train, :], trainYI,
                                 projMat[test, :], testYI)
    return {"inpatient": yi, "outpatient": yo}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("eid", type=int, help="experiment id")
    parser.add_argument("rank", type=int, help="rank to evaluate")
    parser.add_argument("type", help="decomposition type")
    parser.add_argument("-i", "--iters", type=int, default=100)
    parser.add_argument("-ii", type=int, default=10)
    parser.add_argument("-alpha", type=float, help="alpha", default=10)
    parser.add_argument("-th", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=10)
    parser.add_argument("-t", "--testSize", type=float, help="test size",
                        default=0.2)
    parser.add_argument("-s", "--samples", type=int, default=20,
                        help="# of bootstrap samples")
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    inputFile = args.infile
    exptID = args.eid
    testSize = args.testSize
    outerIter = args.iters
    R = args.rank
    s = args.sparsity
    theta = args.theta
    cosReg = args.beta2
    dt = args.type
    alpha = args.alpha
    ii = args.ii

    X, inpatientY, outpatientY, ttss = granitePred.load_split(inputFile,
                                                              testSize,
                                                              args.samples)
    inpatientY = inpatientY.clip(min=0)       # clip y and x
    outpatientY = outpatientY.clip(min=0)
    outfile = open('results/pred-{0}-{1}.json'.format(dt, exptID), 'w')
    RESULT = {'expt': 'pred', 'decomp': dt, 'id': exptID,
              "input": inputFile, "rank": R, "sparsity": s,
              "outerIters": outerIter, "innerIter": ii}
    if dt == "GraniteBCD":
        RESULT["theta"] = theta
        RESULT["cosReg"] = cosReg
    elif dt == "MarbleAPR":
        RESULT["alpha"] = alpha
    pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                initargs=(X, inpatientY, outpatientY, dt, R,
                                          alpha, cosReg, theta, s,
                                          outerIter, ii))
    result = pool.map_async(run_set, ttss).get(9999999)
    for i, sampRes in enumerate(result):
        output = RESULT
        output.update({"type": "inpatient", "sample": i})
        output.update(sampRes["inpatient"])
        outfile.write(json.dumps(output) + "\n")
        output = RESULT
        output.update({"type": "outpatient", "sample": i})
        output.update(sampRes["outpatient"])
        outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
