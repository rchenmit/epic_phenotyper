import numpy as np
import time
from collections import OrderedDict

from gravel import Gravel
import marbleBCD as mbd
import tensorTools as tt
import khatrirao
from tenmat import tenmat
from sptensor import sptensor
from ktensor import ktensor
from dtensor import dtensor


_DEF_MAXITER = 1000
_DEF_DELTATOL = 1e-2
_DEF_REG_L = 0
_DEF_STEP_BETA = 0.5
_DEF_STEP_FACTOR = 1
_DEF_STEP_LAMBDA = 100
_DEF_INNERITER = 3
MAX_INF = 1e10
DIFF_TOL = 1e-10


def grad_ls_A(X, Z, n, Phi):
    """
    Gradient for least squares or Gaussian family with respect
    to mode n
    """
    Xn = tenmat(X, [n])
    Zn = tenmat(Z, [n])
    return np.dot(Zn - Xn, Phi)


def grad_logistic_A(X, Z, n, Phi):
    """
    Gradient for logistic loss
    """
    Xn = tenmat(X, [n])
    Zn = tenmat(Z, [n])
    num = np.exp(Zn)
    denom = 1 + num
    return np.dot(np.divide(num, denom) - Xn, Phi)


def grad_kl_A(X, Z, n, Phi):
    """
    Gradient for KL divergence
    """
    W = mbd.compute_W(X, n, Z)
    return np.dot(W, Phi)


def kruskal_mult_vec(X, v):
    """
    Multiply a kruskal tensor x and a list of vectors v
    Note that the assumption is v is the same length as X
    """
    tmp = np.ones(X.R)  # temporary storage
    for n in range(len(v)):
        tmp = np.multiply(tmp, np.dot(X.U[n].T, v[n]))
    return np.dot(X.lmbda, tmp)


def spten_mult_vec(X, v):
    """
    Multiply a sparse tensor x and a list of vectors v
    Note that the assumption is v is the same length as X
    """
    subs = X.subs
    b = [X.vals.flatten()]  # construct the expanded vectors
    for n in range(len(v)):
        # b_p(n) = a_snp(n)
        b.append(v[subs[:, n]])
    # hadmard product of them all
    w = reduce(lambda x, y: np.multiply(x, y), b)
    return np.sum(w)


def dten_mult_vec(X, v):
    """
    Multiply a dense tensor x and a list of vectors v
    Note that the assumption is v is the same length as X
    """
    Xhat = X
    for n in range(len(v)):
        Xhat = Xhat.ttv(v[n], [n])
    return Xhat


def grad_ls_l(X, Z, A, R):
    """
    Gradient for least squares or Gaussian family for weight
    """
    gradL = np.zeros(R)
    for r in range(R):
        # pick out the rth value
        ar = [A[g][:, r] for g in range(len(A))]
        gradL[r] = kruskal_mult_vec(Z, ar)
        if isinstance(X, sptensor):
            gradL[r] = gradL[r] - spten_mult_vec(X, ar)
        else:
            gradL[r] = gradL[r] - dten_mult_vec(X, ar)
    return gradL


def grad_kl_l(X, Z, A, R):
    """
    The gradient of a weight with respect to KL
    """
    gradL = np.zeros(R)
    for r in range(R):
        # pick out the rth value
        ar = [A[g][:, r] for g in range(len(A))]
        # calculate 1 * value
        oneTens = ktensor(np.ones(R) * 1.0 / R,
                          [np.ones((n, R)) for n in X.shape])
        gradL[r] = kruskal_mult_vec(oneTens, ar)
        if isinstance(X, sptensor):
            # construct a new tensor
            W = sptensor(X.subs, X.vals.flatten() / Z, shape=X.shape)
            gradL[r] = gradL[r] - spten_mult_vec(W, ar)
        else:
            W = dtensor(X._data / Z._data, shape=X.shape)
            gradL[r] = gradL[r] - dten_mult_vec(W, ar)
    return gradL


def grad_logistic_l(X, Z, A, R):
    """
    The gradient of a weight with respect to logistic link
    """
    gradL = np.zeros(R)
    for r in range(R):
        # pick out the rth value
        ar = [A[g][:, r] for g in range(len(A))]
        dZ = Z.to_dtensor()
        term1 = dtensor(np.exp(dZ._data) / (1 + np.exp(dZ._data)), dZ.shape)
        gradL[r] = dten_mult_vec(term1, ar)
        if isinstance(X, sptensor):
            gradL[r] = gradL[r] - spten_mult_vec(X, ar)
        else:
            gradL[r] = gradL[r] - dten_mult_vec(X, ar)
    return gradL


def project_weight(x):
    """
    Clip any values below 0
    """
    x = mbd.fix_A(x)
    return np.array(x).clip(min=0)


class GravelBCD(Gravel):
    reg_l = 0                   # regularization parameter for weights
    innIt = 0                   # the total number of inner iterations
    s = None                    # simplex parameter
    ls_beta = 0                 # line search parameter for step decrease
    weights = None              # the running sum for each of the weights
    loss_scale = None               # the scales of each tensor

    def __init__(self, X, loss, R, memMat, modeDim):
        self.X = X
        self.K = memMat.shape[0]
        self.G = memMat.shape[1]
        self.loss = loss
        self.loss_scale = np.repeat(1, self.K)  # for now set them to be even
        self.R = R
        self.M = memMat
        self.Z = [None for k in range(self.K)]
        self.dim_modes = modeDim
        self.obj_k = [0 for k in range(self.K)]
        self.weights = [0 for k in range(self.K)]

    def initialize(self, fMats=None, biasMats=None):
        self.A = []
        for g in range(self.G):
            tmpMat = tt.rand_l1_matrix(self.dim_modes[g],
                                       self.R + self.K)
            # zero out the columns if this mode is not part of the tensor k
            tensorIdx = np.where(self.M[:, g] == 0)[0]
            for biasR in tensorIdx:
                tmpMat[:, biasR + self.R] = 0
            self.A.append(tmpMat)
        self.lmbda = [np.random.uniform(low=50, high=100, size=self.R + self.K)
                      for k in range(self.K)]

    def _grad_factor_kg(self, k, g):
        """
        Gradient of A(g) / u(g) with respect to tensor k
        This takes into account the different loss functions
        """
        othModes = self._get_negative_n(k, g)
        # calculate Phi = A(-g) * lambda
        Phi = np.dot(khatrirao.khatrirao_array([self.A[i] for i in othModes],
                                               reverse=True),
                     np.diag(self.lmbda[k]))
        n = self.M[k, g] - 1
        if self.loss[k] == "gaussian":
            grad = grad_ls_A(self.X[k], self.Z[k], n, Phi)
        elif self.loss[k] == "poisson":
            grad = grad_kl_A(self.X[k], self.Z[k], n, Phi)
        elif self.loss[k] == "bernoulli":
            grad = grad_logistic_A(self.X[k], self.Z[k], n, Phi)
        return grad

    def _grad_F(self, g):
        """
        Calculate the gradient of the tensor factor g with respect to
        all the k matrices
        """
        tensor_k = self.get_tensor_idx(g)  # get the tensors g belongs to
        gradF = np.zeros(self.A[g].shape)  # set gradient to be 0
        for k in tensor_k:
            gradF += self.loss_scale[k] * self._grad_factor_kg(k, g)
        return gradF

    def compute_total_obj(self):
        """
        Compute the total objective
        """
        ll = super(GravelBCD, self).compute_total_obj()
        ll += self.reg_l * np.sum(self.weights)
        return ll

    def _grad_weight(self, k):
        """
        Calculate the grad for weight
        """
        N_k = self.get_mode_idx(k)  # get all the modes in K
        W = self._calculate_W(0, k)
        gradW = mbd.grad_weight(self.A[N_k[0]], self.A, W, self.R + 1, N_k[1:])
        gradW = np.append(np.repeat(self.reg_l, self.R), 0) + gradW
        return gradW

    def _compute_approx(self, k, x=None, xType=None, n=None):
        """
        Compute the new approximation Z based on the type
        Returns lmbda and a list of matrices
        """
        N_k = self.get_mode_idx(k)
        # default to the original
        AHat = [self.A[j] for j in N_k]
        lHat = self.lmbda[k]
        if xType == "A" and n is not None:
            AHat[n] = x
        elif xType == "l":
            lHat = x
        return lHat, AHat

    def _compute_ll_k(self, rangeK, x, n, xType, update_z):
        """
        Compute the log likelihood for k
        """
        for i, k in enumerate(rangeK):
            lHat, AHat = self._compute_approx(k, x=x, xType=xType, n=n[i])
            approx = self._update_Z(k, lHat, AHat)
            if update_z:
                self.Z[k] = approx
            self.obj_k[k] = self.compute_obj_k(k, approx, lHat, AHat)

    def _update_obj_kg(self, gk, rangeK, x, n, xType, update_z=False):
        # compute the D(X(k), Z(k))
        self._compute_ll_k(rangeK, x, n, xType, update_z)
        # if the type is weight, update the weights
        if xType == 'l':
            for k in rangeK:
                self.weight[k] = np.sum(x[0:self.R])

    def _line_search(self, gradV, t, gk, tensor_k, stepFunc, ll, xType, n):
        """
        Line search which uses the last point and tries to increase it
        """
        x, check2 = stepFunc(gradV, t, gk)          # take a gradient step
        self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType)
        currentLL = self.compute_total_obj()
        while (currentLL > ll + check2 and t > 0):
            t = self.ls_beta * t
            x, check2 = stepFunc(gradV, t, gk)
            self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType)
            currentLL = self.compute_total_obj()
        # only update if necessary
        if currentLL < ll:
            self._update_obj_kg(gk, tensor_k, x=x, n=n, xType=xType,
                                update_z=True)
        return x, currentLL, t

    def update_mode_G(self, g, t, ll, gradFunc, computeFunc):
        """
        Function to do line search for mode g of the matrix
        """
        lastLL = ll
        tensor_k = self.get_tensor_idx(g)
        n = [self.M[k, g] - 1 for k in tensor_k]
        for innIt in range(self.innIt):
            gradVal = gradFunc(g)
            A, ll, t = self._line_search(gradVal, t, g, tensor_k,
                                         computeFunc, ll, xType="A", n=n)
            if t == 0:
                print "Reset t since step size is 0"
                t = _DEF_STEP_FACTOR
                # reset it back to be the old one
                self._update_obj_kg(g, tensor_k, x=self.A[g], n=n, xType="A")
                ll = lastLL
                break
            self.A[g] = A
            if (lastLL - ll < 1e-10):
                print "Minimal function change - break inner"
                break
            lastLL = ll
        return ll, t

    def _compute_fty_A(self, gradA, t, g):
        B, check2 = mbd.compute_fty(gradA, self.A[g], t)

    def _compute_fty_Lambda(self, gradL, t, k):
        # clip the weights to be nonnegative orthant
        return mbd.compute_fty(gradL, self.lmbda[k], t, project_weight)

    def update_weight_K(self, k, tInit, ll):

        """
        Function to do line search for mode g of the matrix
        """
        for innIt in range(self.innIt):
            gradVal = self._grad_weight(k)      # calculate the weight
            l_k, ll, t = self._line_search(gradVal, tInit, k, [k],
                                           self._compute_fty_Lambda,
                                           ll, xType="l", n=[None])
            self.lmbda[k] = l_k
        return ll, t

    def compute_decomp(self, **kwargs):
        # initialization parameters
        max_iters = kwargs.pop('max_iter', _DEF_MAXITER)
        delta_tol = kwargs.pop('delta_tol', _DEF_DELTATOL)
        self.ls_beta = kwargs.pop('step_beta', _DEF_STEP_BETA)
        tInit_L = kwargs.pop('step_lambda', _DEF_STEP_LAMBDA)
        tInit_FM = kwargs.pop('step_factor', _DEF_STEP_FACTOR)
        self.reg_l = kwargs.pop('l_reg', _DEF_REG_L)
        self.innIt = kwargs.pop('inner_iter', _DEF_INNERITER)
        sig = kwargs.pop('sigma_init', None)
        L = kwargs.pop('lmbda_init', None)
        self.s = np.array(kwargs.pop('sparsity', np.repeat(1, self.G)))
        self.initialize()
        if sig is not None:
            self.sigma = sig
        if L is not None:
            self.lmbda = L
        iterInfo = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        # calculate the approximation and obj for each one
        self._update_obj_kg(0, range(self.K), x=None,
                            n=np.repeat(None, self.K),
                            xType=None, update_z=True)
        self.weight = [np.sum(self.lmbda[0:self.R]) for k in range(self.K)]
        ll = self.compute_total_obj()
        for iteration in range(max_iters):
            startIter = time.time()
            iterLL = ll
            for g in range(self.G):
                ll = self.update_mode_G(g, tInit_FM, ll,
                                        self._grad_F,
                                        self._compute_fty_A)
            for k in range(self.K):
                ll = self.update_weight_K(k, tInit_L, ll)
            elapsed = time.time() - startIter
            iterInfo[str(iteration)] = {
                "Time": elapsed,
                "LL": ll
            }
            print "It " + str(iteration) + ":" + str(iterInfo[str(iteration)])
            if abs(iterLL - ll) < delta_tol:
                break
        return iterInfo, ll
