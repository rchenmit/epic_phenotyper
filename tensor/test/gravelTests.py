import numpy as np
import sys
sys.path.append("..")

from sptensor import sptensor
from gravelBCD import GravelBCD


""" Test factorization of sparse matrix """
subs = np.array([[0, 3, 1], [1, 0, 1], [1, 2, 1], [1, 3, 1], [3, 0, 0]])
vals = np.array([[1], [1], [1], [1], [3]])
siz = np.array([5, 5, 2])               # 5x5x2 tensor
# construct the same tensor
X = [sptensor(subs, vals, siz), sptensor(subs, vals, siz)]
memMat = np.array([[1, 2, 3], [1, 2, 3]])
modeDim = [5, 5, 2]
R = 3

gbcd = GravelBCD(X, R, memMat, modeDim)
gbcd.compute_decomp(max_iter=100)
