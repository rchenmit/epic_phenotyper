import numpy as np
import time
from collections import OrderedDict

import brecciaBCD as brec
import marbleBCD as mbd
import graniteBCD as gbd
import tensorTools as tt


_DEF_MAXITER = 100
_DEF_DELTATOL = 1e-2
_DEF_REG_L = 0
_DEF_STEP_BETA = 0.5
_DEF_LINESEARCH_ALPHA = 0.4
_DEF_STEP_FACTOR = 1
_DEF_STEP_PATIENT = 5
_DEF_INNERITER = 10
_DEF_COS_REG = 0
_DEF_THETA = 0.2


def ell_21_prox_update_vec(u, reg_l):
    """
    Proximal update for a vector
    """
    # do a sanity check
    denom = np.linalg.norm(u)
    try:
        scalar = max(0, 1 - (reg_l / denom))
        return scalar * u
    except FloatingPointError:
        return np.zeros(u.shape)


def prox_update_B(B, **kwargs):
    """
    Proximal update for the L2,1 norm of a matrix
    """
    reg_l = kwargs.get("reg_l", 1)
    B = mbd.project_nonnegative_A(B)
    try:
        B[:, :-1] = np.apply_along_axis(ell_21_prox_update_vec,
                                        0, B[:, :-1], reg_l)
    except IndexError:
        print B
        raise ValueError("Exiting because simplex is incorrect")
    return B


def _calculate_l21(A):
    """
    Calculate the L_2,1 matrix norm assuming bias is last col
    """
    return np.sum(np.linalg.norm(A[:, :-1]), axis=0)


def _calculate_ang_term(A, theta):
    """
    Calculate the angular penalty term assuming the bias is last col
    """
    return np.sum(gbd.calc_cosine(A[:, :-1], theta))


class SandstoneAM(brec.BrecciaBCD):
    reg_cos = 0                 # cosine regularization
    theta = None                # angular constraints term
    s = None                    # simplex parameter
    patModes = None             # index corresponding to patient modes
    nonPatModes = None          # indices corresponding to non-patient modes
    cosPenalty = None           # the running cosine penalty for each term

    def __init__(self, X, R, memMat, modeDim):
        super(SandstoneAM, self).__init__(X, R, memMat, modeDim)
        # make the assumption the first K modes = patient modes
        self.patModes = range(self.K)
        self.nonPatModes = range(self.K, self.G)

    def _grad_factor_kg(self, k, g, loc, weight):
        """
        Gradient of A(g) / u(g) with respect to tensor k
        """
        df1 = super(SandstoneAM, self)._grad_factor_kg(k, g, loc, weight)
        return gbd.compute_cos_grad_A(df1, self.A[g][:, :-1],
                                      self.theta[g], self.cos_reg)

    def initialize(self, fMats=None, biasMats=None):
        # create the random factors and set lambda to be 1
        self.A = tt.random_factors(self.dim_modes, self.R + 1)
        self.lmbda = [np.ones(self.R + 1) for k in range(self.K)]

    def compute_total_obj(self):
        """
        Compute the total objective
        """
        ll = super(brec.BrecciaBCD, self).compute_total_obj()
        ll += self.reg_l * np.sum(self.weights)
        ll += self.cos_reg * np.sum(self.cosPenalty)
        return ll

    def _update_obj_kg(self, gk, rangeK, x, n, xType, update_z=False):
        # compute the D(X(k), Z(k))
        self._compute_ll_k(rangeK, x, n, xType, update_z)
        if x is None:
            return
        # update the pertinent part
        if gk in self.patModes:
            self.weights[gk] = _calculate_l21(x)
        else:
            self.cosPenalty[gk] = _calculate_ang_term(x,
                                                      self.theta[gk])

    def _compute_fty_B(self, gradB, t, g):
        A, check2 = mbd.compute_fty(gradB, self.A[g], t, prox_update_B,
                                    reg_l=self.reg_l)
        return A, check2

    def _grad_B(self, g):
        """
        Calculate the gradient of the tensor factor g with respect to
        all the k matrices
        """
        tensor_k = self.get_tensor_idx(g)
        gradF = np.zeros(self.A[g].shape)
        for k in tensor_k:
            gradF += super(SandstoneAM, self)._grad_factor_kg(k, g, self.A,
                                                              self.lmbda[k])
        return gradF

    def compute_decomp(self, **kwargs):
        # initialization parameters
        max_iters = kwargs.pop('max_iter', _DEF_MAXITER)
        delta_tol = kwargs.pop('delta_tol', _DEF_DELTATOL)
        self.ls_beta = kwargs.pop('step_beta', _DEF_STEP_BETA)
        self.ls_alpha = kwargs.pop('step_alpha', _DEF_LINESEARCH_ALPHA)
        self.innIt = kwargs.pop('inner_iter', _DEF_INNERITER)
        self.reg_l = kwargs.pop('reg_l', _DEF_REG_L)
        self.s = np.array(kwargs.pop('sparsity', np.repeat(1, self.G)))
        self.cos_reg = kwargs.pop('cos_reg', _DEF_COS_REG)
        self.theta = kwargs.pop('theta', np.repeat(_DEF_THETA, self.G))
        self.initialize()
        iterInfo = OrderedDict(sorted({}.items(), key=lambda t: t[1]))
        # calculate the approximation and obj for each one
        self._update_obj_kg(0, range(self.K), x=None,
                            n=np.repeat(None, self.K),
                            xType=None, update_z=True)
        self.weight = np.repeat(0, self.G)
        self.cosPenalty = np.repeat(0, self.G)
        # calculate the weights and penalties for each one
        for b in self.patModes:
            self.weight[b] = _calculate_l21(self.A[b])
        for g in self.nonPatModes:
            self.cosPenalty[g] = _calculate_ang_term(self.A[g], self.theta[g])
        tInit = np.append(np.repeat(kwargs.pop('step_patient',
                                               _DEF_STEP_PATIENT), self.K),
                          np.repeat(kwargs.pop('step_factor',
                                               _DEF_STEP_FACTOR), self.G - self.K))  # noqa
        ll = self.compute_total_obj()
        for iteration in range(max_iters):
            startIter = time.time()
            iterLL = ll
            # update the patient modes
            for b in self.patModes:
                ll, tInit[b] = self.update_mode_G(b, tInit[b], ll,
                                                  self._grad_B,
                                                  self._compute_fty_B)
                print "Patient mode:", b, "->", ll
            # update the other non-patient modes
            for g in self.nonPatModes:
                ll, tInit[g] = self.update_mode_G(g, tInit[g], ll,
                                                  self._grad_F,
                                                  self._compute_fty_A)
                print "Non-patient mode:", g, "->", ll
            elapsed = time.time() - startIter
            iterInfo[str(iteration)] = {
                "Time": elapsed,
                "LL": ll,
                "Diff": ll - iterLL
            }
            print "It", str(iteration), ":", str(iterInfo[str(iteration)])
            if abs(iterLL - ll) < delta_tol:
                break
        return iterInfo, ll

    def project_data(self, XHat, **kwargs):
        """
        Project a single tensor onto non-patient modes
        """
        # set the inneriteration
        self.innIt = kwargs.pop('inner_iter', _DEF_INNERITER)
        # copy things away to restore it later
        origA = [self.A[g].copy() for g in range(self.G)]
        origl = [self.lmbda[k].copy() for k in range(self.K)]
        origX = self.X
        origRegL = self.reg_l
        origRegC = self.reg_cos
        origM = self.M.copy()
        origWeight = self.weight.copy()
        origCosP = self.cosPenalty.copy()
        origModeDim = self.dim_modes

        # assumption is first mode is patient mode
        A_new = [np.random.rand(XHat.shape[0], self.R + 1)]
        for g in self.nonPatModes:
            A_new.append(self.A[g])  # add all of them together
        # construct the membership matrix
        M_new = np.arange(1, len(XHat.shape) + 1)
        M_new = M_new.reshape(1, len(XHat.shape))
        l_new = [np.ones(self.R + 1)]
        # update membership matrices and everything
        self.M = M_new
        self.A = A_new
        self.l = l_new
        self.K = 1
        self.G = len(A_new)
        self.X = [XHat]
        self.reg_l = self.reg_cos = 0
        self.dim_modes = XHat.shape
        # compute the new objective based on this new tensor
        self._update_obj_kg(0, range(self.K), x=None,
                            n=np.repeat(None, self.K),
                            xType=None, update_z=True)
        # update the weights for the two to be 0
        self.weight = np.repeat(0, self.G)
        self.cosPenalty = np.repeat(0, self.G)
        ll = self.compute_total_obj()
        tInit = kwargs.pop('step_patient', _DEF_STEP_PATIENT)
        ll, t = self.update_mode_G(0, tInit, ll, self._grad_B,
                                   self._compute_fty_B)
        projMat = self.A[0][:, :-1]
        projMat = tt.norm_rows(projMat)
        # reverse everything for book-keeping purposes
        self.A = origA
        self.lmbda = origl
        self.X = origX
        self.reg_l = origRegL
        self.reg_cos = origRegC
        self.M = origM
        self.K = origM.shape[0]
        self.G = origM.shape[1]
        self.weight = origWeight
        self.cosPenalty = origCosP
        self.dim_modes = origModeDim

        return projMat
