"""
"""
import argparse
import functools
import json
import multiprocessing
import numpy as np
import sys
sys.path.append("..")

import tensorIO as tio                  # noqa
import tensorTools as tt                # noqa
from graniteBCD import GraniteBCD       # noqa

X = None
bestLL = None
llLock = None
exptID = None
R = None
s = None
theta = None


def _init(*args):
    """
    Each pool process calls this initializer.
    Load the array to be populated into that process's global namespace
    """
    global X, bestLL, llLock, exptID, R, s, theta
    X, bestLL, llLock, exptID, R, s, theta = args


def run_sample(beta2, maxIter, resultsDir, tensorIdx, sample):
    np.random.seed()
    spntf = GraniteBCD(X, R=R, alpha=0)
    _, lastLL = spntf.compute_decomp(sparsity=s, cos_reg=beta2,
                                     max_iter=maxIter, theta=theta)
    print (tensorIdx,sample), ":", lastLL
    with llLock:
        if lastLL < bestLL.value and np.isfinite(lastLL):
            spntf.save("{0}/granite-ind-{1}-{2}.dat".format(resultsDir,
                                                            exptID,
                                                            tensorIdx))
            bestLL.value = lastLL
    # Analyze the overlap
    colNNZ = tt.count_ktensor_nnz(spntf.get_signal_factors(), axis=0)
    rowNNZ = tt.count_ktensor_nnz(spntf.get_signal_factors(), axis=1)
    return sample, lastLL, colNNZ, rowNNZ


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="input file")
    parser.add_argument("expt", type=int, help="experiment number")
    parser.add_argument("r", type=int, help="rank")
    parser.add_argument("-o", help="output directory", default="results")
    parser.add_argument("-t", '--theta', nargs='+', type=float, help="theta")
    parser.add_argument("-sp", '--sparsity', nargs='+', type=float, help="s")
    parser.add_argument("-i", '--iters', type=int,
                        help="number of iterations", default=500)
    parser.add_argument("-samp", "--samples", type=int, default=20)
    parser.add_argument("-b2", '--beta2', type=float,
                        help="beta 2", default=10)
    parser.add_argument('-proc', type=int, default=2)
    args = parser.parse_args()

    # experimental setup
    R = args.r
    theta = args.theta
    beta2 = args.beta2
    max_iter = args.iters
    sparsity = args.sparsity
    exptID = args.expt
    inputFile = args.infile
    resultsDir = args.o

    # load class
    multiX, memMat, modeDim, axisDict = tio.load_multi_tensor(inputFile)
    print memMat
    outfile = open('results/granite-ind-{0}.json'.format(exptID), 'w')
    RESULT = {'expt': 'granite-ind', 'id': exptID, "rank": R,
              "input": inputFile, "cosReg": beta2,
              'theta': theta, "s": sparsity, 'maxiter': max_iter}
    for k in range(memMat.shape[0]):
        bestLL = multiprocessing.Value('f', sys.float_info.max)
        llLock = multiprocessing.Lock()
        func = functools.partial(run_sample, beta2, max_iter, resultsDir, k)
        pool = multiprocessing.Pool(processes=args.proc, initializer=_init,
                                    initargs=(multiX[k], bestLL, llLock,
                                              exptID, R, sparsity, theta))
        result = pool.map_async(func, range(args.samples)).get(9999999)
        for sampRes in result:
            output = RESULT
            output.update({"tensorIdx": k, "sample": sampRes[0],
                           "LL": sampRes[1], "colNNZ": sampRes[2],
                           "rowNNZ": sampRes[3]})
            outfile.write(json.dumps(output) + "\n")
    outfile.close()


if __name__ == "__main__":
    main()
