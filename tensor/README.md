# Overview #
A Python-based tensor library based on the [MATLAB Tensor Toolbox](http://www.sandia.gov/~tgkolda/TensorToolbox) and [PyTensor](https://code.google.com/p/pytensor/). The library contains the following tensor factorizations:

* Standard CP (ALS) [Paper](http://epubs.siam.org/doi/pdf/10.1137/07070111X)
* CP factorization using Alternating Poisson Regression [Paper](http://www.sandia.gov/~tgkolda/pubs/pubfiles/ChKo12.pdf)
* Marble (sparse CP-APR) [Paper](http://dl.acm.org/citation.cfm?id=2623658)

# Requirements #
The code requires numpy, scipy, scikit-learn, pymongo, and munkres

# Layout #
The tensor implementations (tensor, ktensor, sptensor) and tensor factorization code are found in the base directory, while the subdirectories contain test and simulation code.